<?php
//INCLUDES
include('connect.php');
$unique = session_id();

if($_POST){
	//ESCAPE STRINGS
	$petsname = $mysqli->real_escape_string($_POST['petsname']);
	$services = $mysqli->real_escape_string($_POST['services']);
	$ihave = $mysqli->real_escape_string($_POST['Pet_Type']);
	$zipcode = $mysqli->real_escape_string($_POST['Zip']);
	$ip = $_SERVER['REMOTE_ADDR'];
	$type = 19;
	
	//QUERY DATABASE TO CHECK IF USERS EXISTS IN DATABASE
	$sql = "SELECT * FROM users WHERE Pet_Name = '$petsname' AND Service_Type = '$services' AND Pet_Type = '$ihave' AND Zip = '$zipcode'";
	$result = $mysqli->query($sql);
	$num_rows = mysqli_num_rows($result);

	//IF USER DOESN'T ALREADY EXIST, ENTER THEM INTO THE DATABASE
	if($num_rows==0){
		$sql_insert = "INSERT INTO users (`unique_key`, `Pet_Name`, `Service_Type`, `Pet_Type`, `Zip`, `Key`, `IP_Address`, `TYPE`) VALUES ('$unique','$petsname', '$services', '$ihave', '$zipcode', '$unique', '$ip', '$type')";
		$result_insert = $mysqli->query($sql_insert);
		
		//IF THE PET TYPE WAS CHOSEN AS 'OTHER' THEN ENTER THE OTHER TEXT INTO THE FIELD
		if($ihave=="Other"){
			$Have_Other = $mysqli->real_escape_string($_POST['Have_Other']);
			if($Have_Other){
				$sql_update_other = "UPDATE users SET Have_Other = '$Have_Other' WHERE Key = '$unique'";
				$result_update_other = $mysqli->query($sql_update_other);
			}
		}
		
		//IF EVERYTHING WORKS, FORWARD TO SECOND FORM PAGE
		if($result_insert){
			header('Location:page-2');
		}
		//IF THERE IS AN ERROR INSERTING THE DATA, GIVE AN ERROR MESSAGE
		else{
			echo "<script>alert('There was an error submitted your info. Please try again.');</script>";
		}
	}
	
	//IF A RECORD WITH ALL THE SAME FIELDS AND VALUES IS IN THE DATABASE GIVE AN ERROR MESSAGE
	else{
		echo "<script>alert('Your records already exist in our database.');</script>";
	}
}
?>