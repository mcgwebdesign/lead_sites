
<?php get_header(); ?>

<div id="content">
	<h2>Error 404 - Page Not Found</h2>
	<p>Please use Search or the sitemap below to find your way...</p>
	<?php wp_list_pages('title_li='); ?>
</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>