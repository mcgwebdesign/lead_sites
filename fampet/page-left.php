<?php
/*
Template Name: Left Content / Right Box
*/
global $h1header;

 $h1header = get_field('title');


?>

<?php get_header(); ?>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/inc/quform/js/plugins.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/inc/quform/js/scripts.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/inc/quform/js/scripts.js"></script>


		<div id="content-inner" style="overflow:hidden;">
				<div id="join-left">

						<div class="quform-outer quform-theme-light-light">
        <form class="quform" action="<?php echo get_template_directory_uri(); ?>/inc/quform/process.php" method="post" enctype="multipart/form-data" onclick="">

            <div class="quform-inner">
                <h2 style="text-align:center;">Please enter your details</h2>

				</p>

                <div class="quform-elements">

					<!-- Begin Text input element -->
					 <div class="quform-element quform-element-text">
					     <div class="quform-spacer">
					         <label for="name" style="float:left;width:125px;" style="float:left;width:125px;">Company: </label>
					         <div class="quform-input">
					             <input id="compname" type="text" name="compname" />
					         </div>
					     </div>
					 </div>
					 <!-- End Text input element -->
					<!-- Begin Text input element -->
					 <div class="quform-element quform-element-text">
					     <div class="quform-spacer">
					         <label for="name" style="float:left;width:125px;">Name: </label>
					         <div class="quform-input">
					             <input id="name" type="text" name="name" />
					         </div>
					     </div>
					 </div>
					 <!-- End Text input element -->
					 <!-- Begin Textarea element -->
					  <div class="quform-element quform-element-textarea quform-huge">
					      <div class="quform-spacer">
					          <label for="message" style="float:left;width:125px;">Address:</label>
					          <div class="quform-input">
					              <textarea id="add" name="add" style="height: 80px;width:250px;"></textarea>
					          </div>
					      </div>
					  </div>
					  <!-- End Textarea element -->
					<!-- Begin Text input element -->
					 <div class="quform-element quform-element-text">
					     <div class="quform-spacer">
					         <label for="name" style="float:left;width:125px;">Phone: </label>
					         <div class="quform-input">
					             <input id="phone" type="text" name="phone" />
					         </div>
					     </div>
					 </div>
					 <!-- End Text input element -->
					<!-- Begin Text input element -->
					 <div class="quform-element quform-element-text">
					     <div class="quform-spacer">
					         <label for="name" style="float:left;width:125px;">Email: </label>
					         <div class="quform-input">
					             <input id="email" type="text" name="email" />
					         </div>
					     </div>
					 </div>
					 <!-- End Text input element -->
					<!-- Begin Text input element -->
					 <div class="quform-element quform-element-text">
					     <div class="quform-spacer">
					         <label for="name" style="float:left;width:125px;">Website: </label>
					         <div class="quform-input">
					             <input id="web" type="text" name="web" />
					         </div>
					     </div>
					 </div>
					 <!-- End Text input element -->					
						<!-- Begin Single select element -->
	                    <div class="quform-element quform-element-select">
	                        <div class="quform-spacer">
	                            <label for="single_select" style="float:left;width:125px;">What Type of Company?</label>
	                            <div class="quform-input">
	                                <select class="quform-tooltip" id="comp" name="comp" style="width: 250px;">
	                                    <option value="">Please select</option>
	                                    <option value="Pet Sitting">Pet Sitting</option>
	                                    <option value="Dog Walking">Dog Walking</option>
										<option value="Pet Boarding">Pet Boarding</option>
										<option value="Dog Training">Dog Training</option>
										<option value="Pet Grooming">Pet Grooming</option>
										<option value="Pet Waste Removal">Pet Waste Removal</option>
										<option value="Doggie Daycare">Doggie Daycare</option>
	                                </select>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- End Single select element --><br>
						<!-- Begin Single select element -->
	                    <div class="quform-element quform-element-select">
	                        <div class="quform-spacer">
	                            <label for="single_select" style="float:left;width:125px;">How Did You Hear About Us?</label>
	                            <div class="quform-input">
	                                <select class="quform-tooltip" id="hear" name="hear" style="width: 250px;">
	                                    <option value="">Please select</option>
	                                    <option value="Google">Google</option>
	                                    <option value="Yahoo">Yahoo</option>
										<option value="Bing">Bing</option>
										<option value="Friend">Friend</option>
										<option value="Other Online">Other Online</option>
										<option value="Other">Other</option>
	                                </select>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- End Single select element --><br>
												<!-- Begin Single select element -->
	                    <div class="quform-element quform-element-select">
	                        <div class="quform-spacer">
	                            <label for="single_select" style="float:left;width:125px;">Is your company insured?</label>
	                            <div class="quform-input">
	                                <select class="quform-tooltip" id="ins" name="ins" style="width: 250px;">
	                                    <option value="">Please select</option>
	                                    <option value="Yes">Yes</option>
	                                    <option value="No">No</option>

	                                </select>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- End Single select element --><br>
						
						<script>
						

jQuery('#ins').change(function(){
  if(jQuery(this).val() == 'Yes'){ // or this.value == 'volvo'
        jQuery("#z").hide();
  }
      if(jQuery(this).val() == 'No'){ // or this.value == 'volvo'
        jQuery("#z").show();
  }
});
						</script>
																		<!-- Begin Single select element -->
	                    <div class="quform-element quform-element-select" style="display:none;" id="z">
	                        <div class="quform-spacer">
	                            <label for="single_select" style="float:left;width:125px;">Are you interested in insuring your business?</label>
	                            <div class="quform-input">
	                                <select class="quform-tooltip" id="ins2" name="ins2" style="width: 250px;">
	                                    <option value="">Please select</option>
	                                    <option value="Yes">Yes</option>
	                                    <option value="No">No</option>

	                                </select>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- End Single select element --><br>
                    	 <!-- Begin Textarea element -->
						  <div class="quform-element quform-element-textarea quform-huge">
						      <div class="quform-spacer">
						          <label for="message" style="float:left;width:125px;">Additional Comments: </label>
						          <div class="quform-input">
						              <textarea id="com" name="com" style="height: 80px;width:250px;"></textarea>
						          </div>
						      </div>
						  </div>
						  <!-- End Textarea element -->
						<div class="quform-element quform-element-captcha">
                        <div class="quform-spacer">
                            <label for="type_the_word">Type the word <span class="quform-required">*</span></label>
                            <div class="quform-input">
                                <input id="type_the_word" type="text" name="type_the_word">
                            </div>
                            <div class="quform-captcha">
                                <div class="quform-captcha-inner">
                                    <img src="<?php echo get_template_directory_uri(); ?>/inc/quform/images/captcha/courier-new-light.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Begin Submit button -->
                    <div class="quform-submit">
                        <div class="quform-submit-inner">
                        <input type="submit" name="btnPWRSubmit" value="Submit" id="btnPWRSubmit" class="submit2" style="width:168px!important;">
                        </div>
                        <div class="quform-loading-wrap"><span class="quform-loading"></span></div>
                    </div>
                    <!-- End Submit button -->
               </div>
           </div>
        </form>
    </div>	
							
							
				</div>
				<div id="join-right">
					<p id="lefttitle" style="text-align:center;margin-right: 60px;
font-family: tahoma, sans-serif;
font-weight: bold;

font-size: 15px;">Hear what our clients are saying! <br>Click <a href=
				"../testimonials">here</a> for testimonials</p>
					<div id="join-inner">
						<div id="join-box">
							<div id="join-text">
						<ul>
					 <li>Receive leads from potential customers that have specified interest directly for your service offering (dog walking, pet sitting, pet boarding, dog training, pet waste removal, doggie daycare & pet grooming).	</li>
					 <li>Advertise your services to customers only within your coverage area – that’s right, the customers we match you with are all located in YOUR existing service area only.	</li>
					 <li>Increase your customer base with quality consumers outside of your existing referral and marketing base.	</li>
					 <li>Increase your web presence and exposure by having tens of thousands of pet owners view your company listing.	</li>
					 <li>Join a network of insured, bonded and established organizations.	</li>
					 <li>No start up costs, no monthly fees and no commitments. You pay only for the valid leads that come your way. Please take a moment to submit your contact information to inquire for pricing. One of our business development specialists will contact you ASAP to discuss the program in more detail.	</li>
					 </ul>

					 	</div>
<div id="privacy-logo" style="position:absolute; bottom:0;margin-bottom: 30px;">
<a href="<?php echo site_url(); ?>/privacy-policy"><img src="../wp-content/themes/fampet/inc/rsc/truste_seal_web.gif"></a>
</div>
					</div>
						</div>
				</div>


		</div><!-- end #content -->
<div id="bottom" style="margin:auto;width:700px;text-align:left;"><?php echo do_shortcode( get_field('bottom') ); ?></div>







<?php get_footer(); ?>