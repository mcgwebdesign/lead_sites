

	<script>
   jQuery(function() {
        jQuery( "#datepicker" ).datepicker({
            showOn: "button",
            buttonImage: "<?php echo get_stylesheet_directory_uri(); ?>/images/calender.jpg",
            buttonImageOnly: true
        });
    });
    </script>    
    <script>
function validateForm()
{   var msg = "";
var x=document.forms["2nd"]["First_Name"].value;
if (x==null || x=="")
  {  msg = msg + "First name must be filled out\n";  }
var x=document.forms["2nd"]["Last_Name"].value;
if (x==null || x=="")
  {  msg = msg + "Last name must be filled out\n";  }
var x=document.forms["2nd"]["Email"].value;
if (x==null || x=="")
  {  msg = msg + "Email must be filled out\n";  }
var x=document.forms["2nd"]["Phone"].value;
var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;  
if (x==null || x=="" || phoneno.test(x) == false)
  {  msg = msg + "Phone must be filled out\n";  }  
if(document.getElementById('Pet_Age_0').checked == false &&document.getElementById('Pet_Age_2').checked == false &&document.getElementById('Pet_Age_1').checked == false &&document.getElementById('Pet_Age_3').checked == false ) {
    msg = msg +"Please Choose Pet Age!\n";}
if(document.getElementById('Pet_Size_0').checked == false &&document.getElementById('Pet_Size_1').checked == false &&document.getElementById('Pet_Size_2').checked == false ) {
    msg = msg +"Please Choose Pet Size!\n";}
if(document.getElementById('radStartDate3').checked == false &&document.getElementById('radStartDate1').checked == false &&document.getElementById('radStartDate2').checked == false &&document.getElementById('radStartDate4').checked == false ) {
    msg = msg +"Please Choose Start Date!\n";}
var x=document.forms["2nd"]["datepicker"].value;
if(document.getElementById('radStartDate1').checked && x.length < 1) {
    msg = msg +"Please Fill In Start Date!\n";}
if(document.getElementById('Number_Of_Pets_0').checked == false &&document.getElementById('Number_Of_Pets_1').checked == false &&document.getElementById('Number_Of_Pets_2').checked == false &&document.getElementById('Number_Of_Pets_3').checked == false ) {
    msg = msg +"Please Select Number of Pets!\n";}    
    
    
if (msg != ""){
alert(msg);
return false;}

}
     function showStartDate(id) {
         var Rid = document.getElementById(id);
         var divStartDate = document.getElementById("divStartDate");
         if (Rid.id == "radStartDate1") {
             divStartDate.style.display = "block";
             document.getElementById("radStartDate2").checked = false;
             document.getElementById("radStartDate3").checked = false;
             document.getElementById("radStartDate4").checked = false;
         }
         else if (Rid.id == "radStartDate2") {
             divStartDate.style.display = "none";
             document.getElementById("radStartDate1").checked = false;
             document.getElementById("radStartDate3").checked = false;
             document.getElementById("radStartDate4").checked = false;
         }
         else if (Rid.id == "radStartDate3") {
             divStartDate.style.display = "none";
             document.getElementById("radStartDate2").checked = false;
             document.getElementById("radStartDate1").checked = false;
             document.getElementById("radStartDate4").checked = false;
         }
         else if (Rid.id == "radStartDate4") {
             divStartDate.style.display = "none";
             document.getElementById("radStartDate2").checked = false;
             document.getElementById("radStartDate3").checked = false;
             document.getElementById("radStartDate1").checked = false;
         }
     }


    </script>
    
    <script type="text/javascript">
jQuery(function() {
      jQuery('#Phone').mask('999-999-9999');
      
   });
</script>

    <form name="2nd" action="http://mcgwebdesign.info/fampet/?page_id=38" method="post" onsubmit="return validateForm()">


<table align="left">

     <tbody>  
                        <tr>
                            <td valign="middle" align="left">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tbody><tr>
                                        <td valign="top" align="left" style="width:475px">
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tbody><tr>
                                                    <td valign="middle" align="left" class="label">
                                                    First Name*:
                                                    </td>
                                                    <td valign="middle" align="left" class="labelControlText">
                                                    <input name="First_Name" type="text" id="First_Name" tabindex="1" class="textbox2" style="width:240px;">
                                                    </td>
                                                </tr>
                                                                                                <tr>
                                                    <td valign="middle" align="left" class="label">
                                                    Last Name*:
                                                    </td>
                                                    <td valign="middle" align="left" class="labelControlText">
                                                    <input name="Last_Name" type="text" id="Last_Name" tabindex="2" class="textbox2" style="width:240px;">
                                                    <span id="RequiredLast_Name" style="display: none; visibility: visible;">Please Enter Your Last Name.</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle" align="left" class="label">
                                                    Email*:
                                                    </td>
                                                    <td valign="middle" align="left" class="labelControlText">
                                                    <input name="Email" type="text" id="Email" tabindex="3" class="textbox2" style="width:240px;">
                                                    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle" align="left" class="label">
                                                    Phone*:
                                                    </td>
                                                    <td valign="middle" align="left" class="labelControlText">
                                                    <input name="Phone" type="text" maxlength="12" id="Phone" tabindex="4" class="textbox2" style="width:240px;">
                                                    
                                                    </td>
                                                </tr>
                                               

                                                <tr>
                                                    <td valign="middle" align="left" class="label">
                                                    City*:
                                                    </td>
                                                    <td valign="middle" align="left" class="labelControlText">
                                                    <input name="City" type="text" value="<?php echo $row_zip['City'] ?>" id="City" class="textbox2" style="width:240px;">
                                                    <span id="RequiredCity" style="display: none; visibility: hidden;">Please Enter Your City.</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle" align="left" class="label">
                                                    State or Province*:
                                                    </td>
                                                    <td valign="middle" align="left" class="labelControlText">
                                                    <input name="State" type="text" value="<?php echo $row_zip['State'] ?>" maxlength="2" id="State" class="textbox2" style="width:240px;">
                                                    <span id="RequiredState" style="display: none; visibility: hidden;">Please Enter Your State or Province.</span>
                                                    </td>
                                                </tr>
<tr>
                                                    <td valign="middle" align="left" class="labelradio">
                                                    Please briefly describe your pet care needs:
                                                    </td>
                                                    
                                                    <td style="width:263px;height:72px; background-image: url(common/images/Newbigtxtbox.jpg); background-repeat:no-repeat" class="labelControlText1">
                                                        <textarea name="txtAdditional" rows="2" cols="20" id="txtAdditional" tabindex="13" class="textbox2" style="height:62px;width:240px;"></textarea>
                                                    </td>
                                                    
                                                </tr>
                                                 <tr>
                                                    <td valign="middle" align="left" class="label">
                                                    Promo Code (optional):
                                                    </td>
                                                    <td valign="middle" align="left" class="labelControlText">
                                                    <input name="txtPromoCode" type="text" id="txtPromoCode" tabindex="8" class="textbox2" style="width:240px;">
                                                    </td>
                                                </tr>


                                              
                                                
                                             
                                             
                                            </tbody></table>
                                            </td>
                                        
                                        <td valign="middle" align="left" style="width:475px">
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tbody>
                                                <tr id="trPetAge">
	<td valign="top" align="left" class="labelradio">
                                                    Age of pet:
                                                    </td>
	<td valign="top" align="left" class="NewFormOption">
                                                        <table id="Pet_Age" style="height:60px;width:100%;">
		<tbody><tr>
			<td><input id="Pet_Age_0" type="radio" name="Pet_Age" value="0-5 months" tabindex="6"><label for="Pet_Age_0"> 0-5 months</label></td><td><input id="Pet_Age_2" type="radio" name="Pet_Age" value="6-11 months"><label for="Pet_Age_2"> 6-11 months</label></td>
		</tr><tr>
			<td><input id="Pet_Age_1" type="radio" name="Pet_Age" value="1-5 years"><label for="Pet_Age_1"> 1-5 years</label></td><td><input id="Pet_Age_3" type="radio" name="Pet_Age" value="6+ years"><label for="Pet_Age_3"> 6+ years</label></td>
		</tr>
	</tbody></table>
                                                     <span id="RequiredPet_Age" style="display: none; visibility: hidden;">Please Select Age Of Your Pet(s).</span>
                                                        
                                                    </td>
</tr>




  <tr id="trNoOfPets">
	<td valign="top" align="left" class="labelradio">
                                                    Number of pets:
                                                    </td>
	<td valign="top" align="left" class="NewFormOption">
                                                        <table id="Number_Of_Pets" style="width:100%;">
		<tbody><tr>
			<td style="
    width: 50px;
"><input id="Number_Of_Pets_0" type="radio" name="Number_Of_Pets" value="1" tabindex="7"><label for="Number_Of_Pets_0"> 1</label></td><td style="
    width: 50px;
"><input id="Number_Of_Pets_1" type="radio" name="Number_Of_Pets" value="2"><label for="Number_Of_Pets_1"> 2</label></td><td style="
    width: 50px;
"><input id="Number_Of_Pets_2" type="radio" name="Number_Of_Pets" value="3"><label for="Number_Of_Pets_2"> 3</label></td><td style="
    width: 50px;
"><input id="Number_Of_Pets_3" type="radio" name="Number_Of_Pets" value="4+"><label for="Number_Of_Pets_3"> 4+</label></td>
		<td style="width:100px;"></td></tr>
	</tbody></table>
                                                        
                                                        
                                                    </td>
</tr>



   <tr id="trSelectStart">
	<td valign="top" align="left" class="labelradio">
                                                    Please select one:
                                                    </td>
	<td valign="top" align="left" class="NewFormOption" style="clear:both;vertical-align:top;">

                                                        <table width="110%">
                                                            <tbody><tr style="clear:both;vertical-align:top;">
                                                                <td align="left" valign="top" style="clear:both;vertical-align:top;">
                                                                <input value="Specific Date" name="Start_Date" type="radio" id="radStartDate1" onclick="showStartDate('radStartDate1');" tabindex="17">&nbsp;I need 
                                                                    <span id="lblService">sitting</span>
                                                                to start on a specific date.
                                                                </td>
                                                            </tr>
                                                            <tr>  
                                                                <td style="vertical-align:top;">
                                                                <div id="divStartDate" style="display:none;vertical-align: top;">
                                                                    <table cellpadding="1" cellspacing="0">
                                                                        <tbody><tr>
                                                                           <td style="width: 135px; float:left; padding-left:15px" align="center">
                                                                                <!--<div id="divStartDate" style="display: none">-->
                                                                            <div id="pnlStartDate" style="background-color: rgb(249, 247, 244); height: 20px; width: 130px; border-top-left-radius: 6px; border-top-right-radius: 6px; border-bottom-right-radius: 6px; border-bottom-left-radius: 6px; border: 1px solid rgb(172, 172, 170);">
		
                                                                            <input name="txtStartDate" id="datepicker" type="text" value="" maxlength="11" disabled class="frmtextboxPD" style="width:100px;">
                                                              
                                                                                 
                                                                           
                                                                             
                                                                            <!-- </div>-->
                                                                            </td>
                                                                            <td style="width: 5px" align="left">
                                                                            </td>
                                                                            <td style="width: 25px" align="left">
                                                                            <!--<div id="divCalImg" style="display: none">-->
                                                                            <!--<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/calender.jpg" id="Cal5" alt="" style="cursor: pointer">-->
                                                                           
                                                                            </td>
                                                                        <td align="left">
                                                                        </td>
                                                                    </tr>
                                                                  </tbody></table>
                                                             </div>
                                                        </td>
                                                    </tr>
                                                            <tr>
                                                            <td align="left" valign="top">
                                                                <input value="Within 15 Days" name="Start_Date" type="radio" id="radStartDate2" onclick="javascript:showStartDate(&#39;radStartDate2&#39;);" tabindex="18">&nbsp;I need 
                                                                    <span id="lblService2">sitting</span>
                                                                to start within 15 days.
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <td align="left" valign="top">
                                                                <input value="Within 30 Days" name="Start_Date" type="radio" id="radStartDate3" onclick="javascript:showStartDate(&#39;radStartDate3&#39;);" tabindex="19">&nbsp;I need 
                                                                    <span id="lblService3">sitting</span>
                                                                to start within 30 days.
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <td align="left" valign="top">
                                                                <input value="Within 60 Days" name="Start_Date" type="radio" id="radStartDate4" onclick="javascript:showStartDate(&#39;radStartDate4&#39;);" tabindex="20">&nbsp;I need 
                                                                    <span id="lblService4">sitting</span>
                                                                     to start within 60 days.
                                                            </td>
                                                            </tr>
                                                 </tbody></table>
                                               </td>
</tr>


                                                                                                <tr id="PetSize">
	<td valign="top" align="left" class="labelradio" style="vertical-align:middle;">
                                                    Size of pet:
                                                    </td>
	<td valign="top" align="left" class="NewFormOption"><br>
                                                    <table id="Pet_Size" style="width:80%;">
		<tbody><tr>
			<td><input id="Pet_Size_0" type="radio" name="Pet_Size" value="Small (less than 20 LBs)"><label for="Pet_Size_0"> Small<br><span class="frmrepeatsmallNew">(less than 20 LBs) </span></label></td><td><input id="Pet_Size_1" type="radio" name="Pet_Size" value="Medium (20-49LBs)"><label for="Pet_Size_1"> Medium<br><span class="frmrepeatsmallNew"> (20-49 LBs) </span></label></td><td><input id="Pet_Size_2" type="radio" name="Pet_Size" value="Large (50+ LBs)"><label for="Pet_Size_2"> Large<br><span class="frmrepeatsmallNew">(50 LBs +)</span></label></td>
		</tr>
	</tbody></table>
                                                         <span id="RequiredPet_Size" style="display: none; visibility: visible;">Please Select Size Of Your Pet(s).</span>
                                                        
                                                    </td>
</tr>

                                                                                               
                                                <tr>
                                                    <td valign="middle" align="left" class="label">
                                                    
                                                    </td>
                                                    
                                                    <td style="height:53px; background-repeat:no-repeat"><br>
                                                     <table width="100%">
                                                    <tbody><tr><td style="vertical-align:bottom;"><input type="submit" name="btnPWRSubmit" value="Submit" onclick="return validate_form();" id="btnPWRSubmit" class="submit2"></td>
                                                    <td style="vertical-align:bottom;padding-left:5px;"><a href="http://localdogwalker.com/Privacypolicy.aspx" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/inc/rsc/truste_seal_web.gif" style="vertical-align:top; padding-top:2px"></a>   </td></tr></tbody></table>
                                                    </td>
                                                    
                                                </tr>
                                            </tbody></table>
                                        </td>
                                        </tr>
                                
                                </tbody></table>
                                
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="left" style=" height:20px">
                            
                                <div id="ValidationSummary1" style="display: none;">

</div>
                            
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="left" class="NewFormBottomHeading">
                            <br><span id="p2-title" style="margin-left: 70px;
font-size: 20px;">YOUR PET'S DETAILS</span>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="left" style="padding-left: 68px;">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tbody><tr>                                        
                                        <td valign="middle" style="width:590px; height:59px; background-image:url(wp-content/themes/fampet/inc/rsc/petDetails.jpg); background-repeat:no-repeat">
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tbody><tr>
                                                    <td class="Newgetquotecontent" style="width:145px" valign="middle">
                                                        Your pet's name:
                                                    </td>
                                                    <td class="Newgetquotecontent" style="width:170px" valign="middle">
                                                        Service you seeking:
                                                    </td>
                                                    <td class="Newgetquotecontent" style="width:80px" valign="middle">
                                                        Pet:
                                                    </td>
                                                    <td class="Newgetquotecontent" style="width:80px" valign="middle">
                                                        Zip Code:
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="middle" class="NewgetquotecontrolContent" style="width:145px">
                                                    <span id="lblPetName"><?php echo $petsname; ?></span>
                                                    </td>
                                                    <td align="left" valign="middle" class="NewgetquotecontrolContent" style="width:170px">
                                                    <span id="lblServiceType"><?php echo $services; ?></span>
                                                    </td>
                                                    <td align="left" valign="middle" class="NewgetquotecontrolContent" style="width:80px">
                                                    <span id="lblPetType"><?php echo $ihave; ?></span>
                                                    </td>
                                                    <td align="left" valign="middle" class="NewgetquotecontrolContent" style="width:80px">
                                                    <span id="lblZipCode"><?php echo $zipcode; ?></span>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        </td>
                                        <td align="left" valign="middle" style="width:390px">&nbsp;
                                        
                                        </td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="left" class="NewFormBottomHeading">
                            <a href="<?php echo home_url(); ?>"><img style="margin-left:75px;" type="image" name="btnPSEdit" id="btnPSEdit" src="<?php echo get_stylesheet_directory_uri(); ?>/images/editButton.png"></a>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="left" style=" height:100px">&nbsp;
                            
                            </td>
                        </tr>
      
      </tbody></table>
      
      </form>
