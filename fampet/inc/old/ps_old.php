<script>
function validateForm()
{
var error;
var msg;

var x=document.forms["2nd"]["First_Name"].value;
if (x==null || x=="")
  {
  error=true;
  alert("First Name must be filled out");
  }
var x=document.forms["2nd"]["Email"].value;
var atpos=x.indexOf("@");
var dotpos=x.lastIndexOf(".");
if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
  {
  error = true;
  alert("Not a valid e-mail address!");
  }
var x=document.forms["2nd"]["Phone"].value;
if (x==null || x=="")
  {
  error=true;
  alert("Not a valid phone number!");
  }
var x=document.forms["2nd"]["Last_Name"].value;
if (x==null || x=="")
  {
  error=true;
  alert("Last name must be filled out");
  }
var x=document.forms["2nd"]["City"].value;
if (x==null || x=="")
  {
  error=true;
  alert("City must be filled out");
  }
var x=document.forms["2nd"]["State"].value;
if (x==null || x=="")
  {
  error=true;
  alert("State must be filled out");
  }
  
  
if (error==true){
	alert(msg);
	return false;
}


}
</script>

<div class="p2">
	<form name="2nd" action="http://mcgwebdesign.info/fampet/?page_id=38" method="post" onsubmit="return validateForm()">
		<div id="full_form">
			<div id="left">
				<label for="firstname">First Name*:</label>
				<input type="text" name="First_Name" /><br /><br />
				<label for="email">Email*:</label>
				<input type="text" name="Email" /><br /><br />
				<label for="phone">Phone*:</label>
				<input type="text" name="Phone" /></br /><br />
				<label for="Pet_Age">Age of pet:</label>
				<input type="radio" name="Pet_Age" value="0-5 Months">0-5 Months
				<input type="radio" name="Pet_Age" value="1-5 Years">1-5 Years<br /><br />
				<input type="radio" name="Pet_Age" value="6-11 Months">6-11 Months
				<input type="radio" name="Pet_Age" value="6+ Years">6+ years<br /><br />
				<label for="Number_Of_Pets">Number of pets:</label>
				<input type="radio" name="Number_Of_Pets" value="1">1
				<input type="radio" name="Number_Of_Pets" value="2">2
				<input type="radio" name="Number_Of_Pets" value="3">3
				<input type="radio" name="Number_Of_Pets" value="4">4<br /><br />
				<label for="Start_Date" style="float:left;">Please select one:</label>
				<div id="selectone_div">
					<input type="radio" name="Start_Date" value="Specific Date" />I need sitting to start on a specific date.<br />
					<input type="radio" name="Start_Date" value="Within 15 Days" />I need sitting to start within 15 days.<br />
					<input type="radio" name="Start_Date" value="Within 30 Days" />I need sitting to start within 30 days.<br />
					<input type="radio" name="Start_Date" value="Within 60 Days" />I need sitting to start within 60 days.<br />
				</div><!--selectone_div-->
			</div><!--left-->
			<div id="right">
				<label for="Last_Name">Last Name*:</label>
				<input type="text" name="Last_Name" /><br /><br />
				<label for="City">City*:</label>
				<input type="text" name="City" value="<?php echo $row_zip['City'] ?>" /><br /><br />
				<label for="State">State or Province*:</label>
				<input type="text" name="State" value="<?php echo $row_zip['State'] ?>" /><br /><br />
				<label for="Pet_Size">Size of pet:</label>
				<input type="radio" name="Pet_Size" value="Small (less than 20 LBs)">Small
				<input type="radio" name="Pet_Size" value="Medium (20-49LBs)">Medium
				<input type="radio" name="Pet_Size" value="Large (50+ LBs)">Large<br /><br />
				<label for="Promo_Code">Promo Code (optional):</label>
				<input type="text" name="Promo_Code" /><br /><br />
				<label for="Comments">Please briefly describe your pet care needs:</label>
				<textarea name="Comments"></textarea>
				<input type="submit" value="submit" />
			</div><!--right-->
		</div><!--full_form-->
	</form>
</div>