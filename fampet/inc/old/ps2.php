<link href="<?php echo get_template_directory_uri(); ?>/inc/rsc/fonts.css" type="text/css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri(); ?>/inc/rsc/layout.css" type="text/css" rel="stylesheet">


 <script type="text/javascript">

     function showStartDate(id) {
         var Rid = document.getElementById(id);
         var divStartDate = document.getElementById("divStartDate");
         if (Rid.id == "radStartDate1") {
             divStartDate.style.display = "block";
             document.getElementById("radStartDate2").checked = false;
             document.getElementById("radStartDate3").checked = false;
             document.getElementById("radStartDate4").checked = false;
         }
         else if (Rid.id == "radStartDate2") {
             divStartDate.style.display = "none";
             document.getElementById("radStartDate1").checked = false;
             document.getElementById("radStartDate3").checked = false;
             document.getElementById("radStartDate4").checked = false;
         }
         else if (Rid.id == "radStartDate3") {
             divStartDate.style.display = "none";
             document.getElementById("radStartDate2").checked = false;
             document.getElementById("radStartDate1").checked = false;
             document.getElementById("radStartDate4").checked = false;
         }
         else if (Rid.id == "radStartDate4") {
             divStartDate.style.display = "none";
             document.getElementById("radStartDate2").checked = false;
             document.getElementById("radStartDate3").checked = false;
             document.getElementById("radStartDate1").checked = false;
         }
     }


     function validate_form() {
         var message = "";
         if (FieldRequired(document.getElementById("First_Name").value) == false) {
             message = "Please Enter Your First Name.";
             alert(message);
             document.getElementById("First_Name").focus();
             return false;
         }

         if (FieldRequired(document.getElementById("Last_Name").value) == false) {
             message = "Please Enter Your Last Name.";
             alert(message);
             document.getElementById("Last_Name").focus();
             return false;
         }
         if (FieldRequired(document.getElementById("Email").value) == false) {
             message = "Please Enter Your Email ID.";
             alert(message);
             document.getElementById("Email").focus();
             return false;
         }
         if (CheckMail(document.getElementById("Email").value) == false) {
             message = "Please Enter Your Email ID in Proper Format.\nEg. Sam@abc.com";
             alert(message);
             document.getElementById("Email").focus();
             return false;
         }
         if (FieldRequired(document.getElementById("City").value) == false) {
             message = "Please Enter Your City.";
             alert(message);
             document.getElementById("City").focus();
             return false;
         }
         if (FieldRequired(document.getElementById("State").value) == false) {
             message = "Please Enter Your State or Province.";
             alert(message);
             document.getElementById("State").focus();
             return false;
         }
         if (checkState(document.getElementById("State").value) == false) {
             alert("Please Enter Two Character State Abbreviation Only,Like: NY");
             document.getElementById("State").focus();
             return false;
         }
         if (FieldRequired(document.getElementById("Phone").value) == false) {
             message = "Please Enter Your Phone Number.";
             alert(message);
             document.getElementById("Phone").focus();
             return false;
         }
         if (phone_number(document.getElementById("Phone").value) == false) {
             alert("Please enter the Phone Number in Format xxx-xxx-xxxx");
             document.getElementById("Phone").focus();
             return false;
         }


         if (document.getElementById("Pet_Age1").checked == false && document.getElementById("Pet_Age2").checked == false && document.getElementById("Pet_Age3").checked == false && document.getElementById("Pet_Age4").checked == false) {
             message = "Please Select Age Of Your Pet(s).\n";
             alert(message);
             return false;
         }
         if (document.getElementById("Pet_Size1").checked == false && document.getElementById("Pet_Size2").checked == false && document.getElementById("Pet_Size3").checked == false) {
             message = "Please Select Size Of Your Pet(s).\n";
             alert(message);
             return false;
         }

         if (document.getElementById("Number_Of_Pets1").checked == false && document.getElementById("Number_Of_Pets2").checked == false && document.getElementById("Number_Of_Pets3").checked == false && document.getElementById("Number_Of_Pets4").checked == false) {
             message = "Please Select Number of Pet(s).\n";
             alert(message);
             return false;
         }
         if (document.getElementById("radStartDate1").checked == false && document.getElementById("radStartDate2").checked == false && document.getElementById("radStartDate3").checked == false && document.getElementById("radStartDate4").checked == false) {
             message = "Please Select One Option For Start date.";
             alert(message);
             return false;
         }
         if (document.getElementById("radStartDate1").checked == true) {
             if (Newcheckdate(document.getElementById("txtStartDate").value) == false) {
                 document.getElementById("txtStartDate").focus();
                 return false;
             }
         }


         return true;

     }


     function checkState(State) {
         var testchar = /[A-Za-z][A-Za-z]+$/;
         if (testchar.test(State) == false) {
             //alert("Please enter alphabets only");
             return false;
         }
     }
  	
 </script>
<link href="<?php echo get_template_directory_uri(); ?>/inc/rsc/WebResource.axd" type="text/css" rel="stylesheet"><style type="text/css"></style></head>



    <form method="post" action="http://mcgwebdesign.info/fampet/?page_id=38" onsubmit="javascript:return WebForm_OnSubmit();" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="">
<input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="/wEPDwUKMTEwOTM5NTExNQ8WAh4IUHJldlBhZ2UFGmh0dHA6Ly9sb2NhbGRvZ3dhbGtlci5jb20vFgICAw9kFhgCAw8PFgIeBFRleHQFGUZpbmQgWW91ciBQZXQgU2l0dGVyIEhlcmVkZAIVDxYCHgdWaXNpYmxlZ2QCFw8WAh8CZ2QCGQ9kFgICAQ9kFgICAQ8QZGQWAGQCGw9kFgICAQ9kFgICAQ8QZGQWAGQCHQ9kFgICAQ9kFgICAQ8QZGQWAGQCHw9kFgICAQ9kFgICAQ8QZGQWAGQCIQ8WAh8CZxYCAgEPZBYKAgMPDxYCHwEFB3NpdHRpbmdkZAIFD2QWAgIDDxYCHglTdGFydERhdGUGYFdqcJdD0EhkAgsPDxYCHwEFB3NpdHRpbmdkZAIPDw8WAh8BBQdzaXR0aW5nZGQCEw8PFgIfAQUHc2l0dGluZ2RkAjkPDxYCHwEFA2FzZGRkAjsPDxYCHwEFC1BldCBTaXR0aW5nZGQCPQ8PFgIfAQUDRG9nZGQCPw8PFgIfAQUFMTIzNDVkZBgBBR5fX0NvbnRyb2xzUmVxdWlyZVBvc3RCYWNrS2V5X18WBQUNcmFkU3RhcnREYXRlMQUNcmFkU3RhcnREYXRlMgUNcmFkU3RhcnREYXRlMwUNcmFkU3RhcnREYXRlNAUJYnRuUFNFZGl0nX/W7+SHSU/IrYpUSDv4b2xUqeFJ5srBLyebY0CkrDk=">
</div>

<script type="text/javascript">
//<![CDATA[
var theForm = document.forms['form1'];
if (!theForm) {
    theForm = document.form1;
}
function __doPostBack(eventTarget, eventArgument) {
    if (!theForm.onsubmit || (theForm.onsubmit() != false)) {
        theForm.__EVENTTARGET.value = eventTarget;
        theForm.__EVENTARGUMENT.value = eventArgument;
        theForm.submit();
    }
}
//]]>
</script>


<script type="text/javascript">
//<![CDATA[
function WebForm_OnSubmit() {
if (typeof(ValidatorOnSubmit) == "function" && ValidatorOnSubmit() == false) return false;
return true;
}
//]]>
</script>

<div class="aspNetHidden">

	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEWHgLhhuT7BQK7zqYkAoTz/boMAsSS7bAIAv6Mn+8CAuCMn+8CAuGMn+8CAuOMn+8CAu7jtYEOAsnCloUBAsjCloUBAsvCloUBAsrCgoYBAsatvOsNZgLg2ZN+ZmZmArvO7scCAtDfxO4EAo6IkZ4HAt/docMEAt7docMEAt3docMEAtCyi60IAtjak8QBAsvg1bcKAtec8OkLAqnQzZ4Pew5I3caeUJZBlTAJpw4+6DYo2KIIyKMwVM+lcx2TlWo=">
</div>
    <script type="text/javascript">
//<![CDATA[
Sys.WebForms.PageRequestManager._initialize('ScriptManager1', 'form1', [], [], [], 90, '');
//]]>
</script>


<script src="<?php echo get_template_directory_uri(); ?>/inc/rsc/WebResource(1).axd" type="text/javascript"></script>


<script src="<?php echo get_template_directory_uri(); ?>/inc/rsc/ScriptResource.axd" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/inc/rsc/ScriptResource(1).axd" type="text/javascript"></script>


<script src="<?php echo get_template_directory_uri(); ?>/inc/rsc/ScriptResource(2).axd" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/inc/rsc/ScriptResource(3).axd" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/inc/rsc/ScriptResource(4).axd" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/inc/rsc/ScriptResource(5).axd" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/inc/rsc/ScriptResource(6).axd" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/inc/rsc/ScriptResource(7).axd" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/inc/rsc/ScriptResource(8).axd" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/inc/rsc/ScriptResource(9).axd" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/inc/rsc/ScriptResource(10).axd" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/inc/rsc/ScriptResource(11).axd" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/inc/rsc/ScriptResource(12).axd" type="text/javascript"></script>      
 <table align="left">

     <tbody><tr>
                             <td valign="middle" align="left">
                              <table cellpadding="0" cellspacing="0">
                                    <tbody><tr>
                                        <td class="Body_Top_Left">
                                        </td>
                                        <td class="Body_Top_Middle_PD_Form" rowspan="2">
                                           
                                        </td>
                                        <td class="Body_Top_Right" colspan="2">
                                        </td>
                                    </tr>
                                   
                                </tbody></table>
                                
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="left" class="NewFormHeading">
                            <a name="pwpd">Step 2 of 2: Please enter your details</a>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="left">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tbody><tr>                                        
                                        <td valign="middle" align="left" class="NewFormcontent">
                                        Why do we need this? Your personal information will be sent in a safe and secure environment only to the pet care companies so that they can contact you with there service offering. Read our <a href="http://localdogwalker.com/PrivacyPolicy.aspx" target="_blank">Privacy Policy</a>.
                                        </td>
                                        <td valign="middle" align="left" style="width:315px">
                                        </td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="left" style=" height:10px">
                            </td>
                            </tr>
                        
                        <tr>
                            <td valign="middle" align="left">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tbody><tr>
                                        <td valign="middle" align="left" style="width:475px">
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tbody><tr>
                                                    <td valign="middle" align="left" class="NewFormQuestion">
                                                    First Name*:
                                                    </td>
                                                    <td valign="middle" align="left" class="NewFormQuestionControlText">
                                                    <input name="First_Name" type="text" id="First_Name" tabindex="1" class="NewfrmtextboxPD" style="width:240px;">
                                                    <span id="RequiredFirst_Name" style="display: none; visibility: visible;">Please Enter Your First Name.</span>
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle" align="left" class="NewFormQuestion">
                                                    Email*:
                                                    </td>
                                                    <td valign="middle" align="left" class="NewFormQuestionControlText">
                                                    <input name="Email" type="text" id="Email" tabindex="3" class="NewfrmtextboxPD" style="width:240px;">
                                                    <span id="RequiredEmail" style="display: none; visibility: visible;">Please Enter Your Email ID.</span>
                                                    <span id="RegularEmail" style="display: none; visibility: hidden;">Please Enter Your Valid Email ID</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle" align="left" class="NewFormQuestion">
                                                    Phone*:
                                                    </td>
                                                    <td valign="middle" align="left" class="NewFormQuestionControlText">
                                                    <input name="Phone" type="text" maxlength="12" id="Phone" tabindex="4" class="NewfrmtextboxPD" style="width:240px;">
                                                    <span id="RequiredPhone" style="display: none; visibility: visible;">Please Enter Your Phone Number.</span>
                                                    <span id="RegularPhone" style="display: none; visibility: hidden;">Please enter the Phone Number in Format xxx-xxx-xxxx</span>
                                                    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle" align="left" style="height:20px">
                                                    </td>
                                                    <td valign="middle" align="left" style="height:20px; padding-left:7px">
                                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/Ph-No.png">
                                                    </td>
                                                </tr>

                                                <tr id="trPetAge">
	<td valign="top" align="left" class="NewFormQuestionradio">
                                                    Age of pet:
                                                    </td>
	<td valign="top" align="left" class="NewFormOption">
                                                        <table id="Pet_Age" style="height:60px;width:100%;">
		<tbody><tr>
			<td><input id="Pet_Age_0" type="radio" name="Pet_Age" value="0"><label for="Pet_Age_0">0-5 months</label></td><td><input id="Pet_Age_2" type="radio" name="Pet_Age" value="2"><label for="Pet_Age_2">6-11 months</label></td>
		</tr><tr>
			<td><input id="Pet_Age_1" type="radio" name="Pet_Age" value="1"><label for="Pet_Age_1">1-5 years</label></td><td><input id="Pet_Age_3" type="radio" name="Pet_Age" value="3"><label for="Pet_Age_3">6+ years</label></td>
		</tr>
	</tbody></table>
                                                     <span id="RequiredPet_Age" style="display: none; visibility: hidden;">Please Select Age Of Your Pet(s).</span>
                                                        
                                                    </td>
</tr>

                                                <tr id="trNoOfPets">
	<td valign="top" align="left" class="NewFormQuestionradio">
                                                    Number of pets:
                                                    </td>
	<td valign="top" align="left" class="NewFormOption">
                                                        <table id="Number_Of_Pets" style="width:100%;">
		<tbody><tr>
			<td><input id="Number_Of_Pets_0" type="radio" name="Number_Of_Pets" value="1"><label for="Number_Of_Pets_0">1</label></td><td><input id="Number_Of_Pets_1" type="radio" name="Number_Of_Pets" value="2"><label for="Number_Of_Pets_1">2</label></td><td><input id="Number_Of_Pets_2" type="radio" name="Number_Of_Pets" value="3"><label for="Number_Of_Pets_2">3</label></td><td><input id="Number_Of_Pets_3" type="radio" name="Number_Of_Pets" value="4+"><label for="Number_Of_Pets_3">4+</label></td>
		</tr>
	</tbody></table>
                                                        <span id="RequiredNumber_Of_Pets" style="display: none; visibility: hidden;">Please Select Number Of Your Pet(s).</span>
                                                        
                                                    </td>
</tr>

                                                
                                                <!--for pet waste removal -->
                                                
                                                
                                                <!--end-->
                                                <!--Dog walker-->
                                                
                                                
                                                <!--end-->
                                                <tr id="trSelectStart">
	<td valign="top" align="left" class="NewFormQuestionradio">
                                                    Please select one:
                                                    </td>
	<td valign="top" align="left" class="NewFormOption" style="clear:both;vertical-align:top;">

                                                        <table width="110%">
                                                            <tbody><tr style="clear:both;vertical-align:top;">
                                                                <td align="left" valign="top" style="clear:both;vertical-align:top;">
                                                                <input value="radStartDate1" name="Start_Date" type="radio" id="radStartDate1" onclick="javascript:showStartDate(&#39;radStartDate1&#39;);" tabindex="17">&nbsp;I need 
                                                                    <span id="lblService">sitting</span>
                                                                to start on a specific date.
                                                                </td>
                                                            </tr>
                                                            <tr>  
                                                                <td style="vertical-align:top;">
                                                                <div id="divStartDate" style="vertical-align: top; display: block;">
                                                                    <table cellpadding="1" cellspacing="0">
                                                                        <tbody><tr>
                                                                           <td style="width: 135px; float:left; padding-left:15px" align="center">
                                                                                <!--<div id="divStartDate" style="display: none">-->
                                                                            <div id="pnlStartDate" style="background-color: rgb(249, 247, 244); height: 20px; width: 130px; border-top-left-radius: 6px; border-top-right-radius: 6px; border-bottom-right-radius: 6px; border-bottom-left-radius: 6px; border: 1px solid rgb(172, 172, 170);">
		
                                                                            <input name="txtStartDate" id="datepicker" type="text" value="" maxlength="11" class="frmtextboxPD" style="width:100px;">
                           <p>Date: <input type="text" id="datepicker" /></p>                                                     
                                                                                 
                                                                            
	<div id="CalendarExtender1_container" class="ajax__calendar" style="visibility: hidden; position: absolute; left: 462px; top: 455px; z-index: 1000; display: none;"><div id="CalendarExtender1_popupDiv" class="ajax__calendar_container"><div id="CalendarExtender1_header" class="ajax__calendar_header"><div><div id="CalendarExtender1_prevArrow" class="ajax__calendar_prev"></div></div><div><div id="CalendarExtender1_nextArrow" class="ajax__calendar_next"></div></div><div><div id="CalendarExtender1_title" class="ajax__calendar_title">July, 2013</div></div></div><div id="CalendarExtender1_body" class="ajax__calendar_body"><div id="CalendarExtender1_days" class="ajax__calendar_days" style="position: absolute; left: 0px; top: 0px;"><table id="CalendarExtender1_daysTable" cellpadding="0" cellspacing="0" border="0" style="margin: auto;"><thead id="CalendarExtender1_daysTableHeader"><tr id="CalendarExtender1_daysTableHeaderRow"><td><div class="ajax__calendar_dayname">Su</div></td><td><div class="ajax__calendar_dayname">Mo</div></td><td><div class="ajax__calendar_dayname">Tu</div></td><td><div class="ajax__calendar_dayname">We</div></td><td><div class="ajax__calendar_dayname">Th</div></td><td><div class="ajax__calendar_dayname">Fr</div></td><td><div class="ajax__calendar_dayname">Sa</div></td></tr></thead><tbody id="CalendarExtender1_daysBody"><tr><td class="ajax__calendar_other"><div id="CalendarExtender1_day_0_0" class="ajax__calendar_day" title="Sunday, June 30, 2013">30</div></td><td class=""><div id="CalendarExtender1_day_0_1" class="ajax__calendar_day" title="Monday, July 01, 2013">1</div></td><td class="ajax__calendar_active"><div id="CalendarExtender1_day_0_2" class="ajax__calendar_day" title="Tuesday, July 02, 2013">2</div></td><td class=""><div id="CalendarExtender1_day_0_3" class="ajax__calendar_day" title="Wednesday, July 03, 2013">3</div></td><td class=""><div id="CalendarExtender1_day_0_4" class="ajax__calendar_day" title="Thursday, July 04, 2013">4</div></td><td class=""><div id="CalendarExtender1_day_0_5" class="ajax__calendar_day" title="Friday, July 05, 2013">5</div></td><td class=""><div id="CalendarExtender1_day_0_6" class="ajax__calendar_day" title="Saturday, July 06, 2013">6</div></td></tr><tr><td class=""><div id="CalendarExtender1_day_1_0" class="ajax__calendar_day" title="Sunday, July 07, 2013">7</div></td><td class=""><div id="CalendarExtender1_day_1_1" class="ajax__calendar_day" title="Monday, July 08, 2013">8</div></td><td class=""><div id="CalendarExtender1_day_1_2" class="ajax__calendar_day" title="Tuesday, July 09, 2013">9</div></td><td class=""><div id="CalendarExtender1_day_1_3" class="ajax__calendar_day" title="Wednesday, July 10, 2013">10</div></td><td class=""><div id="CalendarExtender1_day_1_4" class="ajax__calendar_day" title="Thursday, July 11, 2013">11</div></td><td class=""><div id="CalendarExtender1_day_1_5" class="ajax__calendar_day" title="Friday, July 12, 2013">12</div></td><td class=""><div id="CalendarExtender1_day_1_6" class="ajax__calendar_day" title="Saturday, July 13, 2013">13</div></td></tr><tr><td class=""><div id="CalendarExtender1_day_2_0" class="ajax__calendar_day" title="Sunday, July 14, 2013">14</div></td><td class=""><div id="CalendarExtender1_day_2_1" class="ajax__calendar_day" title="Monday, July 15, 2013">15</div></td><td class=""><div id="CalendarExtender1_day_2_2" class="ajax__calendar_day" title="Tuesday, July 16, 2013">16</div></td><td class=""><div id="CalendarExtender1_day_2_3" class="ajax__calendar_day" title="Wednesday, July 17, 2013">17</div></td><td class=""><div id="CalendarExtender1_day_2_4" class="ajax__calendar_day" title="Thursday, July 18, 2013">18</div></td><td class=""><div id="CalendarExtender1_day_2_5" class="ajax__calendar_day" title="Friday, July 19, 2013">19</div></td><td class=""><div id="CalendarExtender1_day_2_6" class="ajax__calendar_day" title="Saturday, July 20, 2013">20</div></td></tr><tr><td class=""><div id="CalendarExtender1_day_3_0" class="ajax__calendar_day" title="Sunday, July 21, 2013">21</div></td><td class=""><div id="CalendarExtender1_day_3_1" class="ajax__calendar_day" title="Monday, July 22, 2013">22</div></td><td class=""><div id="CalendarExtender1_day_3_2" class="ajax__calendar_day" title="Tuesday, July 23, 2013">23</div></td><td class=""><div id="CalendarExtender1_day_3_3" class="ajax__calendar_day" title="Wednesday, July 24, 2013">24</div></td><td class=""><div id="CalendarExtender1_day_3_4" class="ajax__calendar_day" title="Thursday, July 25, 2013">25</div></td><td class=""><div id="CalendarExtender1_day_3_5" class="ajax__calendar_day" title="Friday, July 26, 2013">26</div></td><td class=""><div id="CalendarExtender1_day_3_6" class="ajax__calendar_day" title="Saturday, July 27, 2013">27</div></td></tr><tr><td class=""><div id="CalendarExtender1_day_4_0" class="ajax__calendar_day" title="Sunday, July 28, 2013">28</div></td><td class=""><div id="CalendarExtender1_day_4_1" class="ajax__calendar_day" title="Monday, July 29, 2013">29</div></td><td class=""><div id="CalendarExtender1_day_4_2" class="ajax__calendar_day" title="Tuesday, July 30, 2013">30</div></td><td class=""><div id="CalendarExtender1_day_4_3" class="ajax__calendar_day" title="Wednesday, July 31, 2013">31</div></td><td class="ajax__calendar_other"><div id="CalendarExtender1_day_4_4" class="ajax__calendar_day" title="Thursday, August 01, 2013">1</div></td><td class="ajax__calendar_other"><div id="CalendarExtender1_day_4_5" class="ajax__calendar_day" title="Friday, August 02, 2013">2</div></td><td class="ajax__calendar_other"><div id="CalendarExtender1_day_4_6" class="ajax__calendar_day" title="Saturday, August 03, 2013">3</div></td></tr><tr><td class="ajax__calendar_other"><div id="CalendarExtender1_day_5_0" class="ajax__calendar_day" title="Sunday, August 04, 2013">4</div></td><td class="ajax__calendar_other"><div id="CalendarExtender1_day_5_1" class="ajax__calendar_day" title="Monday, August 05, 2013">5</div></td><td class="ajax__calendar_other"><div id="CalendarExtender1_day_5_2" class="ajax__calendar_day" title="Tuesday, August 06, 2013">6</div></td><td class="ajax__calendar_other"><div id="CalendarExtender1_day_5_3" class="ajax__calendar_day" title="Wednesday, August 07, 2013">7</div></td><td class="ajax__calendar_other"><div id="CalendarExtender1_day_5_4" class="ajax__calendar_day" title="Thursday, August 08, 2013">8</div></td><td class="ajax__calendar_other"><div id="CalendarExtender1_day_5_5" class="ajax__calendar_day" title="Friday, August 09, 2013">9</div></td><td class="ajax__calendar_other"><div id="CalendarExtender1_day_5_6" class="ajax__calendar_day" title="Saturday, August 10, 2013">10</div></td></tr></tbody></table></div><div id="CalendarExtender1_months" class="ajax__calendar_months" style="display: none; visibility: hidden;"><table id="CalendarExtender1_monthsTable" cellpadding="0" cellspacing="0" border="0" style="margin: auto;"><tbody id="CalendarExtender1_monthsBody"><tr><td><div id="CalendarExtender1_month_0_0" class="ajax__calendar_month"><br>Jan</div></td><td><div id="CalendarExtender1_month_0_1" class="ajax__calendar_month"><br>Feb</div></td><td><div id="CalendarExtender1_month_0_2" class="ajax__calendar_month"><br>Mar</div></td><td><div id="CalendarExtender1_month_0_3" class="ajax__calendar_month"><br>Apr</div></td></tr><tr><td><div id="CalendarExtender1_month_1_0" class="ajax__calendar_month"><br>May</div></td><td><div id="CalendarExtender1_month_1_1" class="ajax__calendar_month"><br>Jun</div></td><td><div id="CalendarExtender1_month_1_2" class="ajax__calendar_month"><br>Jul</div></td><td><div id="CalendarExtender1_month_1_3" class="ajax__calendar_month"><br>Aug</div></td></tr><tr><td><div id="CalendarExtender1_month_2_0" class="ajax__calendar_month"><br>Sep</div></td><td><div id="CalendarExtender1_month_2_1" class="ajax__calendar_month"><br>Oct</div></td><td><div id="CalendarExtender1_month_2_2" class="ajax__calendar_month"><br>Nov</div></td><td><div id="CalendarExtender1_month_2_3" class="ajax__calendar_month"><br>Dec</div></td></tr></tbody></table></div><div id="CalendarExtender1_years" class="ajax__calendar_years" style="display: none; visibility: hidden;"><table id="CalendarExtender1_yearsTable" cellpadding="0" cellspacing="0" border="0" style="margin: auto;"><tbody id="CalendarExtender1_yearsBody"><tr><td><div id="CalendarExtender1_year_0_0" class="ajax__calendar_year"></div></td><td><div id="CalendarExtender1_year_0_1" class="ajax__calendar_year"></div></td><td><div id="CalendarExtender1_year_0_2" class="ajax__calendar_year"></div></td><td><div id="CalendarExtender1_year_0_3" class="ajax__calendar_year"></div></td></tr><tr><td><div id="CalendarExtender1_year_1_0" class="ajax__calendar_year"></div></td><td><div id="CalendarExtender1_year_1_1" class="ajax__calendar_year"></div></td><td><div id="CalendarExtender1_year_1_2" class="ajax__calendar_year"></div></td><td><div id="CalendarExtender1_year_1_3" class="ajax__calendar_year"></div></td></tr><tr><td><div id="CalendarExtender1_year_2_0" class="ajax__calendar_year"></div></td><td><div id="CalendarExtender1_year_2_1" class="ajax__calendar_year"></div></td><td><div id="CalendarExtender1_year_2_2" class="ajax__calendar_year"></div></td><td><div id="CalendarExtender1_year_2_3" class="ajax__calendar_year"></div></td></tr></tbody></table></div></div><div class="ajax__calendar_invalid"><div id="CalendarExtender1_today" class="ajax__calendar_footer ajax__calendar_today">Today: June 29, 2013</div></div></div></div></div>
                                                                             
                                                                            <!-- </div>-->
                                                                            </td>
                                                                            <td style="width: 5px" align="left">
                                                                            </td>
                                                                            <td style="width: 25px" align="left">
                                                                            <!--<div id="divCalImg" style="display: none">-->
                                                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/calender.jpg" id="Cal5" alt="" style="cursor: pointer">
                                                                           
                                                                            </td>
                                                                        <td align="left">
                                                                        </td>
                                                                    </tr>
                                                                  </tbody></table>
                                                             </div>
                                                        </td>
                                                    </tr>
                                                            <tr>
                                                            <td align="left" valign="top">
                                                                <input value="radStartDate2" name="Start_Date" type="radio" id="radStartDate2" onclick="javascript:showStartDate(&#39;radStartDate2&#39;);" tabindex="18">&nbsp;I need 
                                                                    <span id="lblService2">sitting</span>
                                                                to start within 15 days.
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <td align="left" valign="top">
                                                                <input value="radStartDate3" name="Start_Date" type="radio" id="radStartDate3" onclick="javascript:showStartDate(&#39;radStartDate3&#39;);" tabindex="19">&nbsp;I need 
                                                                    <span id="lblService3">sitting</span>
                                                                to start within 30 days.
                                                            </td>
                                                            </tr>
                                                            <tr>
                                                            <td align="left" valign="top">
                                                                <input value="radStartDate4" name="Start_Date" type="radio" id="radStartDate4" onclick="javascript:showStartDate(&#39;radStartDate4&#39;);" tabindex="20">&nbsp;I need 
                                                                    <span id="lblService4">sitting</span>
                                                                     to start within 60 days.
                                                            </td>
                                                            </tr>
                                                 </tbody></table>
                                               </td>
</tr>

                                            </tbody></table>
                                            </td>
                                        
                                        <td valign="middle" align="left" style="width:475px">
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tbody><tr>
                                                    <td valign="middle" align="left" class="NewFormQuestion1">
                                                    Last Name*:
                                                    </td>
                                                    <td valign="middle" align="left" class="NewFormQuestionControlText1">
                                                    <input name="Last_Name" type="text" id="Last_Name" tabindex="2" class="NewfrmtextboxPD" style="width:240px;">
                                                    <span id="RequiredLast_Name" style="display: none; visibility: visible;">Please Enter Your Last Name.</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle" align="left" class="NewFormQuestion1">
                                                    City*:
                                                    </td>
                                                    <td valign="middle" align="left" class="NewFormQuestionControlText1">
                                                    <input name="City" type="text" value="SCHENECTADY" id="City" class="NewfrmtextboxPD" style="width:240px;">
                                                    <span id="RequiredCity" style="display: none; visibility: hidden;">Please Enter Your City.</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle" align="left" class="NewFormQuestion1">
                                                    State or Province*:
                                                    </td>
                                                    <td valign="middle" align="left" class="NewFormQuestionControlText1">
                                                    <input name="State" type="text" value="NY" maxlength="2" id="State" class="NewfrmtextboxPD" style="width:240px;">
                                                    <span id="RequiredState" style="display: none; visibility: hidden;">Please Enter Your State or Province.</span>
                                                    </td>
                                                </tr>
                                                <tr id="PetSize">
	<td valign="top" align="left" class="NewFormQuestionradio1">
                                                    Size of pet:
                                                    </td>
	<td valign="top" align="left" class="NewFormOption">
                                                    <table id="Pet_Size" style="width:80%;">
		<tbody><tr>
			<td><input id="Pet_Size_0" type="radio" name="Pet_Size" value="1"><label for="Pet_Size_0">Small<br><span class="frmrepeatsmallNew">(less than 20 LBs) </span></label></td><td><input id="Pet_Size_1" type="radio" name="Pet_Size" value="2"><label for="Pet_Size_1">Medium<br><span class="frmrepeatsmallNew"> (20-49 LBs) </span></label></td><td><input id="Pet_Size_2" type="radio" name="Pet_Size" value="3"><label for="Pet_Size_2">Large<br><span class="frmrepeatsmallNew">(50 LBs +)</span></label></td>
		</tr>
	</tbody></table>
                                                         <span id="RequiredPet_Size" style="display: none; visibility: visible;">Please Select Size Of Your Pet(s).</span>
                                                        
                                                    </td>
</tr>

                                                <tr>
                                                    <td valign="middle" align="left" class="NewFormQuestion1">
                                                    Promo Code (optional):
                                                    </td>
                                                    <td valign="middle" align="left" class="NewFormQuestionControlText1">
                                                    <input name="txtPromoCode" type="text" id="txtPromoCode" tabindex="8" class="NewfrmtextboxPD" style="width:240px;">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="middle" align="left" class="NewFormQuestionradio1">
                                                    Please briefly describe your pet care needs:
                                                    </td>
                                                    
                                                    <td style="width:263px;height:72px; background-image: url(common/images/Newbigtxtbox.jpg); background-repeat:no-repeat" class="NewFormQuestionControlText1">
                                                        <textarea name="txtAdditional" rows="2" cols="20" id="txtAdditional" tabindex="13" class="NewfrmtextboxPD" style="height:62px;width:240px;"></textarea>
                                                    </td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td valign="middle" align="left" class="NewFormQuestion1">
                                                    
                                                    </td>
                                                    
                                                    <td style="height:53px; background-repeat:no-repeat">
                                                     <table width="100%">
                                                    <tbody><tr><td style="vertical-align:bottom;"><input type="submit" name="btnPWRSubmit" value="Submit" onclick="javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions(&quot;btnPWRSubmit&quot;, &quot;&quot;, true, &quot;btnSubmit&quot;, &quot;&quot;, false, false))" id="btnPWRSubmit" class="submit2"></td>
                                                    <td style="vertical-align:bottom;padding-left:5px;"><a href="http://localdogwalker.com/Privacypolicy.aspx" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/inc/rsc/truste_seal_web.gif" style="vertical-align:top; padding-top:2px"></a>   </td></tr></tbody></table>
                                                    </td>
                                                    
                                                </tr>
                                            </tbody></table>
                                        </td>
                                        </tr>
                                
                                </tbody></table>
                                
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="left" style=" height:20px">
                            
                                <div id="ValidationSummary1" style="display: none;">

</div>
                            
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="left" class="NewFormBottomHeading">
                            YOUR PET'S DETAILS
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="left" style="padding-left: 68px;">
                                <table cellpadding="0" cellspacing="0" width="100%">
                                    <tbody><tr>                                        
                                        <td valign="middle" style="width:590px; height:59px; background-image:url(common/images/petDetails.jpg); background-repeat:no-repeat">
                                            <table cellpadding="0" cellspacing="0" width="100%">
                                                <tbody><tr>
                                                    <td class="Newgetquotecontent" style="width:145px" valign="middle">
                                                        Your pet's name:
                                                    </td>
                                                    <td class="Newgetquotecontent" style="width:170px" valign="middle">
                                                        Service you seeking:
                                                    </td>
                                                    <td class="Newgetquotecontent" style="width:80px" valign="middle">
                                                        Pet:
                                                    </td>
                                                    <td class="Newgetquotecontent" style="width:80px" valign="middle">
                                                        Zip Code:
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="middle" class="NewgetquotecontrolContent" style="width:145px">
                                                    <span id="lblPetName">asd</span>
                                                    </td>
                                                    <td align="left" valign="middle" class="NewgetquotecontrolContent" style="width:170px">
                                                    <span id="lblServiceType">Pet Sitting</span>
                                                    </td>
                                                    <td align="left" valign="middle" class="NewgetquotecontrolContent" style="width:80px">
                                                    <span id="lblPetType">Dog</span>
                                                    </td>
                                                    <td align="left" valign="middle" class="NewgetquotecontrolContent" style="width:80px">
                                                    <span id="lblZipCode">12345</span>
                                                    </td>
                                                </tr>
                                            </tbody></table>
                                        </td>
                                        <td align="left" valign="middle" style="width:390px">&nbsp;
                                        
                                        </td>
                                    </tr>
                                </tbody></table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="left" class="NewFormBottomHeading">
                            <input type="image" name="btnPSEdit" id="btnPSEdit" src="<?php echo get_stylesheet_directory_uri(); ?>/images/editButton.png">
                            </td>
                        </tr>
                        <tr>
                            <td valign="middle" align="left" style=" height:100px">&nbsp;
                            
                            </td>
                        </tr>
      
      </tbody></table>



     </div>
    
    <!-- Body ends --> 
  </div>
  <!-- Center container ends --> 
  <!-- Streched Footer starts -->
 

</div>
<!-- Page ends --> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/inc/rsc/jquery.stylish-select.min.js"></script> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/inc/rsc/jquery.custom.js"></script>
    
<script type="text/javascript">
//<![CDATA[
var Page_ValidationSummaries =  new Array(document.getElementById("ValidationSummary1"));
var Page_Validators =  new Array(document.getElementById("RequiredFirst_Name"), document.getElementById("RequiredEmail"), document.getElementById("RegularEmail"), document.getElementById("RequiredPhone"), document.getElementById("RegularPhone"), document.getElementById("RequiredPet_Age"), document.getElementById("RequiredNumber_Of_Pets"), document.getElementById("RequiredLast_Name"), document.getElementById("RequiredCity"), document.getElementById("RequiredState"), document.getElementById("RequiredPet_Size"));
//]]>
</script>

<script type="text/javascript">
//<![CDATA[
var RequiredFirst_Name = document.all ? document.all["RequiredFirst_Name"] : document.getElementById("RequiredFirst_Name");
RequiredFirst_Name.controltovalidate = "First_Name";
RequiredFirst_Name.errormessage = "Please Enter Your First Name.";
RequiredFirst_Name.validationGroup = "btnSubmit";
RequiredFirst_Name.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
RequiredFirst_Name.initialvalue = "";
var RequiredEmail = document.all ? document.all["RequiredEmail"] : document.getElementById("RequiredEmail");
RequiredEmail.controltovalidate = "Email";
RequiredEmail.errormessage = "Please Enter Your Email ID.";
RequiredEmail.validationGroup = "btnSubmit";
RequiredEmail.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
RequiredEmail.initialvalue = "";
var RegularEmail = document.all ? document.all["RegularEmail"] : document.getElementById("RegularEmail");
RegularEmail.controltovalidate = "Email";
RegularEmail.errormessage = "Please Enter Your Valid Email ID";
RegularEmail.validationGroup = "btnSubmit";
RegularEmail.evaluationfunction = "RegularExpressionValidatorEvaluateIsValid";
RegularEmail.validationexpression = "\\w+([-+.\']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
var RequiredPhone = document.all ? document.all["RequiredPhone"] : document.getElementById("RequiredPhone");
RequiredPhone.controltovalidate = "Phone";
RequiredPhone.errormessage = "Please Enter Your Phone Number.";
RequiredPhone.validationGroup = "btnSubmit";
RequiredPhone.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
RequiredPhone.initialvalue = "";
var RegularPhone = document.all ? document.all["RegularPhone"] : document.getElementById("RegularPhone");
RegularPhone.controltovalidate = "Phone";
RegularPhone.errormessage = "Please enter the Phone Number in Format xxx-xxx-xxxx";
RegularPhone.validationGroup = "btnSubmit";
RegularPhone.evaluationfunction = "RegularExpressionValidatorEvaluateIsValid";
RegularPhone.validationexpression = "([0-9]{3})+\\-([0-9]{3})+\\-([0-9]{4})";
var RequiredPet_Age = document.all ? document.all["RequiredPet_Age"] : document.getElementById("RequiredPet_Age");
RequiredPet_Age.controltovalidate = "Pet_Age";
RequiredPet_Age.errormessage = "Please Select Age Of Your Pet(s).";
RequiredPet_Age.validationGroup = "btnSubmit";
RequiredPet_Age.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
RequiredPet_Age.initialvalue = "";
var RequiredNumber_Of_Pets = document.all ? document.all["RequiredNumber_Of_Pets"] : document.getElementById("RequiredNumber_Of_Pets");
RequiredNumber_Of_Pets.controltovalidate = "Number_Of_Pets";
RequiredNumber_Of_Pets.errormessage = "Please Select Number Of Your Pet(s).";
RequiredNumber_Of_Pets.validationGroup = "btnSubmit";
RequiredNumber_Of_Pets.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
RequiredNumber_Of_Pets.initialvalue = "";
var RequiredLast_Name = document.all ? document.all["RequiredLast_Name"] : document.getElementById("RequiredLast_Name");
RequiredLast_Name.controltovalidate = "Last_Name";
RequiredLast_Name.errormessage = "Please Enter Your Last Name.";
RequiredLast_Name.validationGroup = "btnSubmit";
RequiredLast_Name.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
RequiredLast_Name.initialvalue = "";
var RequiredCity = document.all ? document.all["RequiredCity"] : document.getElementById("RequiredCity");
RequiredCity.controltovalidate = "City";
RequiredCity.errormessage = "Please Enter Your City.";
RequiredCity.validationGroup = "btnSubmit";
RequiredCity.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
RequiredCity.initialvalue = "";
var RequiredState = document.all ? document.all["RequiredState"] : document.getElementById("RequiredState");
RequiredState.controltovalidate = "State";
RequiredState.errormessage = "Please Enter Your State or Province.";
RequiredState.validationGroup = "btnSubmit";
RequiredState.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
RequiredState.initialvalue = "";
var RequiredPet_Size = document.all ? document.all["RequiredPet_Size"] : document.getElementById("RequiredPet_Size");
RequiredPet_Size.controltovalidate = "Pet_Size";
RequiredPet_Size.errormessage = "Please Select Size Of Your Pet(s).";
RequiredPet_Size.validationGroup = "btnSubmit";
RequiredPet_Size.evaluationfunction = "RequiredFieldValidatorEvaluateIsValid";
RequiredPet_Size.initialvalue = "";
var ValidationSummary1 = document.all ? document.all["ValidationSummary1"] : document.getElementById("ValidationSummary1");
ValidationSummary1.showmessagebox = "True";
ValidationSummary1.showsummary = "False";
ValidationSummary1.validationGroup = "btnSubmit";
//]]>
</script>


<script type="text/javascript">
//<![CDATA[

var Page_ValidationActive = false;
if (typeof(ValidatorOnLoad) == "function") {
    ValidatorOnLoad();
}

function ValidatorOnSubmit() {
    if (Page_ValidationActive) {
        return ValidatorCommonOnSubmit();
    }
    else {
        return true;
    }
}
        
document.getElementById('ValidationSummary1').dispose = function() {
    Array.remove(Page_ValidationSummaries, document.getElementById('ValidationSummary1'));
}

document.getElementById('RequiredFirst_Name').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('RequiredFirst_Name'));
}

document.getElementById('RequiredEmail').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('RequiredEmail'));
}

document.getElementById('RegularEmail').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('RegularEmail'));
}

document.getElementById('RequiredPhone').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('RequiredPhone'));
}

document.getElementById('RegularPhone').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('RegularPhone'));
}

document.getElementById('RequiredPet_Age').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('RequiredPet_Age'));
}

document.getElementById('RequiredNumber_Of_Pets').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('RequiredNumber_Of_Pets'));
}
Sys.Application.add_init(function() {
    $create(Sys.Extended.UI.CalendarBehavior, {"button":$get("Cal5"),"format":"dd-MMM-yyyy","id":"CalendarExtender1","popupPosition":4,"startDate":"Sun, 30 Jun 2013 10:55:16 GMT"}, null, null, $get("txtStartDate"));
});
Sys.Application.add_init(function() {
    $create(Sys.Extended.UI.RoundedCornersBehavior, {"BorderColor":"#ACACAA","Radius":6,"id":"RoundedCornersExtender8"}, null, null, $get("pnlStartDate"));
});

document.getElementById('RequiredLast_Name').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('RequiredLast_Name'));
}

document.getElementById('RequiredCity').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('RequiredCity'));
}

document.getElementById('RequiredState').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('RequiredState'));
}

document.getElementById('RequiredPet_Size').dispose = function() {
    Array.remove(Page_Validators, document.getElementById('RequiredPet_Size'));
}
//]]>
</script>
</form>