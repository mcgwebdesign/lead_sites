<?php
/*
Template Name: Main Form Template
*/
?>

<?php
$unique = session_id();
// Grab SRC Parameters
$_GET_lower = array_change_key_case($_GET, CASE_LOWER);
$src = $_GET_lower['arc'];
if (!$src) $src=get_bloginfo('description');
$subid = $_GET_lower['subid'];
$pubid = $_GET_lower['pubid'];
?>



<?php get_header(home); ?>

<!-- Write script for radio buttons-->
<script>
function validateForm()
{	var msg = "";
var x=document.forms["main"]["petsname"].value;
if (x==null || x=="")
  {  msg = msg + "Pet name must be filled out\n";  }
var x=document.forms["main"]["Zip"].value;
if (x==null || x=="")
  {  msg = msg + "You must enter a valid Zip Code!\n";  }
var x=document.forms["main"]["Have_Other"].value;
if(document.getElementById('other').checked && x.length < 1) {
	msg = msg +"Please Fill In Pet Type!\n";}
if(document.getElementById('other').checked == false &&document.getElementById('cat').checked == false &&document.getElementById('dog').checked == false ) {
	msg = msg +"Please Choose Pet Type!\n";}

if (msg != ""){
alert(msg);
return false;}
}
</script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/main_form.js"></script>




		<div id="content" style="min-height:662px;margin-top:-120px;position:relative;">





			<div id="left">



				<div id="orderform">
					<div id="title"> <h4>FILL BELOW TO START</h4> </div>
					<form name="main" action="<?php echo home_url(); ?>/2nd-page/#page2-content" method="post" style="margin-left:18px;padding-top:20px;" onsubmit="return validateForm()">

					<div class="box"><label for="petsname">What is your pet's name?</label><br />

					<input name="petsname" type="text" class="textbox" /><br /></div>

					<div class="box"><label for="services">What services are you seeking?</label><br />

					<select name="services" id="services">
							<?php the_field('select_box_code'); ?>


					</select><br /></div>

					<div class="box"><label for="ihave">I have a:</label><br />

						<input type="radio" name="Pet_Type" id="dog" value="Dog" style="margin-top:5px;"> Dog&nbsp;&nbsp;

						<input type="radio" name="Pet_Type" id="cat" value="Cat"> Cat<br />

						<input type="radio" name="Pet_Type" id="other" value="Other" style="margin-top:10px;"> Other &nbsp; <input type="text" name="Have_Other" class="textboxs"/><br /></div>

				<div class="box">	<label for="zipcode">Enter Zip Code or Postal Code:</label>

					<input type="text" name="Zip" id="zipz" class="textbox"/><br />
					<input type="text" name="SRC" class="textbox" style="display:none;" value="<?php echo $src;?>"/>
					<input type="text" name="SubID" class="textbox" style="display:none;" value="<?php echo $subid;?>"/>
					<input type="text" name="PubID" class="textbox" style="display:none;" value="<?php echo $pubid;?>"/>
					<input type="text" name="unique" class="textbox" style="display:none;" value="<?php echo $unique;?>"/>
					
					
					
					</div><div class="submitbox">
<input style="margin-top: 8px;margin-left: -6px;" type="submit" value="Get Quotes !" onclick="return validate_form();" class="submit"></div>
				</form>

<div id="privacy-logo">
<div id="6614d0cf-1fad-4c74-889a-0f3443137bbb"> <script type="text/javascript" src="//privacy-policy.truste.com/privacy-seal/Dice-Solutions-LLC/asc?rid=6614d0cf-1fad-4c74-889a-0f3443137bbb"></script><a href="//privacy.truste.com/privacy-seal/Dice-Solutions-LLC/validation?rid=d152df82-1e09-4637-8568-7b7da1c642f2" title="TRUSTe online privacy certification" target="_blank"><img style="border: none" src="//privacy-policy.truste.com/privacy-seal/Dice-Solutions-LLC/seal?rid=d152df82-1e09-4637-8568-7b7da1c642f2" alt="TRUSTe online privacy certification"/></a></div>
</div>



				</div>







			</div>



			<div id="right">







							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



									



									<h2><?php the_field('page_title'); ?></h2>



						



										<?php the_content(); ?>



							<?php endwhile; endif; ?>



			</div>



			<div id="bottom" style="clear:both;"><?php echo do_shortcode( get_field('bottom') ); ?></div>



		</div><!-- end #content -->















<?php get_footer(); ?>