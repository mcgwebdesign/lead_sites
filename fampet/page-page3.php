<?php
/*

Template Name: Page3

*/
?>
<?php
include('connect.php');
$unique = session_id();


// Get all post data
if($_POST){
	$First_Name = $mysqli->real_escape_string($_POST['First_Name']);
	$Last_Name = $mysqli->real_escape_string($_POST['Last_Name']);
	$Email = $mysqli->real_escape_string($_POST['Email']);
	$Phone1 = $mysqli->real_escape_string($_POST['Phone']);
	$Phone2 = str_replace('-', '', $Phone1);
	$Phone = str_replace(' ', '', $Phone2);
	$Pet_Age = $mysqli->real_escape_string($_POST['Pet_Age']);
	$Number_Of_Pets = $mysqli->real_escape_string($_POST['Number_Of_Pets']);
	$Start_Date = $mysqli->real_escape_string($_POST['Start_Date']);
	if ($Start_Date == 'Specific Date') $date = $mysqli->real_escape_string($_POST['txtStartDate']);
	$City = $mysqli->real_escape_string($_POST['City']);
	$State = $mysqli->real_escape_string($_POST['State']);
	$Pet_Size = $mysqli->real_escape_string($_POST['Pet_Size']);
	$Promo_Code = $mysqli->real_escape_string($_POST['Promo_Code']);
	$Comments = $mysqli->real_escape_string($_POST['Comments']);
	$when = $mysqli->real_escape_string($_POST['when']);
	$desc = $mysqli->real_escape_string($_POST['desc']);
	$often = $mysqli->real_escape_string($_POST['often']);
	$API_Action = "submitLead";
	$landing_page = get_bloginfo ( 'description' );
	$Pet_Name = $mysqli->real_escape_string($_POST['petsname']);
	$Pet_Type = $mysqli->real_escape_string($_POST['ihave']);
	$Pet_Type_Other_Value = $mysqli->real_escape_string($_POST['other']);
	$Service_Type = $mysqli->real_escape_string($_POST['services']);
	$type = $mysqli->real_escape_string($_POST['type']);
	$zipcode = $mysqli->real_escape_string($_POST['zipcode']);
	$Zip = $zipcode;
	$src = $mysqli->real_escape_string($_POST['src']);
	$subid = $mysqli->real_escape_string($_POST['subid']);
	$pubid = $mysqli->real_escape_string($_POST['pubid']);
	$ip =  $mysqli->real_escape_string($_POST['ip']);
	$unique =  $mysqli->real_escape_string($_POST['unique']);

	// Redirect on blank post
	if ($First_Name == "" || $Email == "")
	{
		header("HTTP/1.1 303 See Other");
		$urlz = home_url();
		header("Location: " .  $urlz );
		die();
	}

	//Add Completed User to DB
	$sql_insert = 
	"INSERT INTO users (`Pet_Name`, `Service_Type`, `Pet_Type`, `Zip`, `uKey`, `IP_Address`, `TYPE`, `SRC`, `SubID`, `PubID`,`Last_Name`,`First_Name`,`Email`,`Phone`,`Pet_Age` ,`Number_Of_Pets`,`Start_Date`,`City`,`State` ,`Pet_Size` ,`Promo_Code`,  `Comments` ,`API_Action` , `Landing_Page`,`date` ,`when` , `desc` , `often` ) 
	 VALUES ('$Pet_Name', '$Service_Type', '$Pet_Type', '$zipcode', '$unique', '$ip', '$type', '$src', '$subid', '$pubid','$Last_Name','$First_Name','$Email','$Phone','$Pet_Age','$Number_Of_Pets','$Start_Date','$City','$State','$Pet_Size','$Promo_Code','$Comments','$API_Action', '$landing_page','$date','$when','$desc','$often')";

	$result_insert = $mysqli->query($sql_insert);
}


// Page Title
global $h1header;

 $h1header = get_field('title');
if ($Service_Type == "Pet Sitting") 
		{ $h1header = 'Find Your Pet Sitter Here!'; 
			$leadtype .= "S";
		}
	elseif ($Service_Type == "Dog Walking") 
		{ $h1header = 'Find Your Dog Walker Here!'; $leadtype .= "D";}
	elseif ($Service_Type == "Pet Boarding") 
		{ $h1header = 'Find Your Pet Boarder Here!'; $leadtype .= "PB";}
	elseif ($Service_Type == "Doggie Daycare") 
		{ $h1header = 'Find Doggie Daycare Here!'; $leadtype .= "DD";}
	elseif ($Service_Type == "Dog Training") 
		{ $h1header = 'Find Your Dog Trainer Here!'; $leadtype .= "T";}
	elseif ($Service_Type == "Pet Waste Removal") 
		{ $h1header = 'Find Pet Waste Removal Here!'; $leadtype .= "WR";}
	elseif ($Service_Type == "Pet Grooming") 
		{ $h1header = 'Find Your Pet Groomer Here!'; $leadtype .= "G";}


//part 1 from page4
$showform = "display:none;";
if ($Pet_Type =="Dog" || $Pet_Type == "Cat"){
$showform = "display:block;";
}

?>

<?php get_header(); ?>



		<div id="content">
<div id="upsell">
<span class="thankyouheading">Last Questions</span><br><br>
<form action="<?php echo home_url(); ?>/results" method="POST">
Would you like to receive FREE discounts of up to 90% on Dog & Cat deals from Coupaw.com?<br /><br>
<table>
                                        <tbody><tr>
                                            <td style="vertical-align:middle;clear:both;">
                                            <table>
                                                <tbody><tr style="vertical-align:middle;clear:both;">
                                                <td class="getQuesRadio" style="width:70px;clear:both;">
                                                   <input type="radio" name="coupaw_upsell" value="yes" /> Yes
                                                </td>
                                                </tr>
                                                <tr>
                                                <td class="getQuesRadio" style="vertical-align:middle;clear:both;padding-top: 10px;">
                                                   <input type="radio" name="coupaw_upsell" value="no" /> No
                                                </td>
                                                
                                                </tr>
                                            </tbody></table>
                                            </td>
                                            <td>
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/Coupaw_cat_dog.jpg" height="80" width="80" alt="img">
                                            </td>
                                        </tr>
                                           
                                        </tbody></table><br>
										<script type="text/javascript"> 
function show() {
    var insurance = document.getElementById("dob");
    insurance.style.display = "block";
     insurance = document.getElementById("gender");
    insurance.style.display = "block";
     insurance = document.getElementById("breed");
    insurance.style.display = "block";
    insurance = document.getElementById("fixed");
    insurance.style.display = "block";
	insurance = document.getElementById("dis");
    insurance.style.display = "block";
}
function hide() {
    var insurance = document.getElementById("dob");
    insurance.style.display = "none";
     insurance = document.getElementById("gender");
    insurance.style.display = "none";
     insurance = document.getElementById("breed");
    insurance.style.display = "none";
    insurance = document.getElementById("fixed");
    insurance.style.display = "none";
	insurance = document.getElementById("dis");
    insurance.style.display = "none";
}
</script>
<script>
   jQuery(function() {
        jQuery( "#datepicker" ).datepicker({
            showOn: "both",
            buttonImage: "http://test1.petsitting.com/wp-content/themes/petsitting/images/calender.jpg",
            buttonImageOnly: true
        });
    });
	
	
	
		
    </script>    
	

		<div style="<?php echo $showform; ?>">								<!-- Insurance-->
	Would you be interested in speaking to a pet insurance professional about saving money on pet’s vet bills?<br /><br>						
					<table>
    <tbody>
        <tr>
            <td style="vertical-align:middle;clear:both;">
                <table>
                    <tbody>
                        <tr style="vertical-align:middle;clear:both;">
                            <td class="getQuesRadio" style="width:70px;clear:both;">
                                <input type="radio" name="insurance_upsell" value="yes" onclick="show()" /> Yes</td>
                        </tr>
                        <tr>
                            <td class="getQuesRadio" style="vertical-align:middle;clear:both;padding-top: 10px;">
                                <input type="radio" name="insurance_upsell" value="no" onclick="hide()" /> No</td>
                        </tr>
                        <tr id="dob" style="display:none;">
                            <td class="getQuesRadio" style="vertical-align:middle;clear:both;padding-top: 10px;">What is your pet’s birthday?  (approximate as close as possible) 
                                <br>
								<div id="pnlStartDate" style="margin-top: 7px;background-color: rgb(249, 247, 244); height: 20px; width: 120px; border-top-left-radius: 6px; border-top-right-radius: 6px; border-bottom-right-radius: 6px; border-bottom-left-radius: 6px; border: 1px solid rgb(172, 172, 170);">
		
                                                                         
                                 <input name="txtStartDate" id="datepicker" type="text" value="" maxlength="11" readonly class="frmtextboxPD" style="width:90px;vertical-align: top;padding-top: 6px;padding-left:7px;">
                           </div>
						   </td>
                        </tr>
                        <tr id="breed" style="display:none;">
                            <td class="getQuesRadio" style="vertical-align:middle;clear:both;padding-top: 10px;">What is the breed of your pet?
                                <br>
                                <select name="breed" id="breed">
								<option value=""> </option>
								<?php
								
								if ($Pet_Type =="Dog"){
$sql = "SELECT master FROM `coreg_breeds_dog`";
$result = $mysqli->query($sql);

while($rowa = $result->fetch_array())
{
 echo '<option value="'. $rowa['master']. '">'. $rowa['master'] . '</option>';
}

								}
								if ($Pet_Type == "Cat"){
$sql = "SELECT master FROM `coreg_breeds_cat`";
$result = $mysqli->query($sql);

while($a = $result->fetch_array())
{
 echo '<option value="'. $a['master']. '">'. $a['master'] . '</option>';
}
								}
								
								
								?>
                                   
                                </select>
                            </td>
                        </tr>
                        <tr id="gender" style="display:none;">
                            <td class="getQuesRadio" style="vertical-align:middle;clear:both;padding-top: 10px;">What is the gender of your pet?
                                <br>
                                <select name="gender" id="gender">
									<option value=""> </option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </td>
                        </tr>
                        <tr id="fixed" style="display:none;">
                            <td class="getQuesRadio" style="vertical-align:middle;clear:both;padding-top: 10px;">Is your pet spayed or neutered?
                                <br>
                                <select name="fixed" id="fixed">
								<option value=""> </option>
                                    <option value="yes"> Yes</option>
                                    <option value="no"> No</option>
                                </select>
                            </td>
                        </tr>
						<tr id="dis" style="display:none;">
                            <td class="getQuesRadio" style="vertical-align:middle;clear:both;padding-top: 10px;">Does your pet have Diabetes, Cushings Disease, or FeLV/FIV?
                                <br>
                                <select name="dis" id="dis">
								<option value="no"> No</option>
                                    <option value="yes"> Yes</option>
                                    
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td></td>
        </tr>
    </tbody>
</table>		
							
							
<br></div>
<input type="image" name="btnFinish" id="btnFinish" src="<?php echo get_stylesheet_directory_uri(); ?>/images/finalize.png">
</form>
</div>

		</div><!-- end #content -->



<?php



//Make Adjustments for Canada
$ziptext = "&Zip=";
$statetext = "&State=";
if( $type > 26){
	$ziptext = "&Postal_Code=";
	$statetext = "&Province=";

	if ($Start_Date == "Within 60 Days"){
		$Start_Date = "Price Shopping";
		
	}
}


//part 2 from page4
//MAKE BOBERDOO API CALL WITH cURL 

$testflag = 'Test_Lead=1&';
//$testflag = '';


$url = "http://leads.petsitting.com/genericPostlead.php?" . $testflag . "TYPE=".urlencode($type)."&IP_Address=".urlencode($ip)."&SRC=".urlencode($src)."&Landing_Page=".urlencode($landing_page)."&First_Name=".urlencode($First_Name)."&Last_Name=".urlencode($Last_Name)."&City=".urlencode($City). $statetext .urlencode($State). $ziptext . urlencode($Zip)."&Email=".urlencode($Email)."&Phone=".urlencode($Phone)."&Pet_Name=".urlencode($Pet_Name)."&Pet_Type=".urlencode($Pet_Type)."&Pet_Age=".urlencode($Pet_Age)."&Number_Of_Pets=".urlencode($Number_Of_Pets)."&Service_Type=".urlencode($Service_Type)."&Pet_Size=".urlencode($Pet_Size);

//email values
$msgform .="<tr><td>Name:</td><td>".$First_Name. " " . $Last_Name ."</td>";
$msgform .="<tr><td>City & State:</td><td>".$City. ", " . $State ."</td>";
$msgform .="<tr><td>Zip:</td><td>".$Zip . "</td>";
$msgform .="<tr><td>Phone:</td><td>".$Phone . "</td>";
$msgform .="<tr><td>Email:</td><td>".$Email . "</td>";
$msgform .="<tr><td>Type of Pet:</td><td>".$Pet_Type . "</td>";
if($Pet_Type_Other_Value != ''){
    $url .= "&Pet_Type_Other_Value=".urlencode($Pet_Type_Other_Value);
    $msgform .="<tr><td>Pet Type:</td><td>".$Pet_Type_Other_Value . "</td>";
}
$msgform .="<tr><td>Pet Name:</td><td>".$Pet_Name . "</td>";
$msgform .="<tr><td>Size:</td><td>".$Pet_Size . "</td>";
$msgform .="<tr><td>Age:</td><td>".$Pet_Age . "</td>";
$msgform .="<tr><td>Start Date:</td><td>".$Start_Date . "</td>";
if( $date != ''){
    $url .= "&Specific_Date=".urlencode($date);
    $msgform .="<tr><td>Start at Specific Date:</td><td>".$date . "</td>";
}
$msgform .="<tr><td># pets:</td><td>".$Number_Of_Pets . "</td>";

$msgform .="<tr><td>Lead Registration Date:</td><td>". date("m/d/Y") . "</td>";


//Optional codes are appended if filled in
if($Start_Date != ''){
    $url .= "&Start_Date=".urlencode($Start_Date);
}
if( $Promo_Code != ''){
    $url .= "&Promo_Code=".urlencode($Promo_Code);
}
if( $Comments != ''){
    $url .= "&Comments=".urlencode($Comments);
    $msgform .="<tr><td>Comments:</td><td>".$Comments . "</td>";
}
if( $subid != ''){
    $url .= "&Sub_ID=".urlencode($subid);
}
if( $pubid != ''){
    $url .= "&Pub_ID=".urlencode($pubid);
}
if( $when != ''){
    $url .= "&Walk_Time_Frame=".urlencode($when);
}
if( $when != '' && intval($Number_Of_Pets ) > 1){
    $url .= "&Multiple_Dogs=".urlencode('Yes');
}
if( $when != '' && intval($Number_Of_Pets ) <= 1){
    $url .= "&Multiple_Dogs=".urlencode('No');
}
if( $desc != ''){
    $url .= "&Home_Type=".urlencode($desc);
}
if( $often != ''){
    $url .= "&Service_Frequency=".urlencode($often);
}


//wrapper to prevent blank submissions
 if ($First_Name != ''){
 $url;
$curl = curl_init($url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$data = curl_exec($curl);
}

$raw_xml = $mysqli->real_escape_string($data);
$xml = simplexml_load_string($data);

//part 3 from page 4

    $sql = "INSERT INTO xml_response (api_request, response, datetime, unique_key) VALUES ('$url', '$raw_xml', NOW(), '$unique')";
    $result = $mysqli->query($sql);


//part 5
// insert lead no into email

foreach($xml->children() as $child)
  {
  $err[]= $child->getName();
  $leadno =$child;
  break;
  }


$leadtype = $landing_page;


if ($err[0] != 'error' || $err[0] != 'Error'){
    $i = -1;
	  foreach($xml->children() as $tier)
      {
        foreach($tier->children() as $client){
            $i = $i + 1;
        }
      }

	  
	  
	  $sql1 = "SELECT * FROM users WHERE `Email` = '$Email'";
	$result1 = $mysqli->query($sql1);
	$num_rowsx = mysqli_num_rows($result1);
	  
	  if ($First_Name != '' || $num_rowsx == 1  ){

	  if (strpos($leadno, 'Error') === FALSE){
		
		echo 'email went out';
	//  include('inc/mail_internal.php');
}
}


}




?>




<?php get_footer(); ?>