<?php
/*
Template Name: Page2
*/
?>
<?php
//INCLUDES
include('connect.php');
$unique = session_id();

// ReGen Unique if duplicate found
$sql = "SELECT * FROM users WHERE uKey = '$unique'";
$result = $mysqli->query($sql);
$num_rows = mysqli_num_rows($result);
if ($num_rows > 0){
	session_regenerate_id();
	$unique = session_id();
}

if($_POST){
	//ESCAPE STRINGS
	$petsname = $mysqli->real_escape_string($_POST['petsname']);
	$services = $mysqli->real_escape_string($_POST['services']);
	$ihave = $mysqli->real_escape_string($_POST['Pet_Type']);
	$zipcode = $mysqli->real_escape_string($_POST['Zip']);
	$src = $mysqli->real_escape_string($_POST['SRC']);
	$subid = $mysqli->real_escape_string($_POST['SubID']);
	$pubid = $mysqli->real_escape_string($_POST['PubID']);
	
	$ip = $_SERVER['REMOTE_ADDR'];
	
	//QUERY ZIP CODE DATABASE FOR CITY NAME
	$sql_zip = "SELECT * FROM zipcodes WHERE ZipCode='$zipcode'";
	$result_zip = $mysqli->query($sql_zip);
	$row_zip = mysqli_fetch_array($result_zip);
	$numzip_rows = mysqli_num_rows($result_zip);

	if($numzip_rows==0){
		echo "<script language='javascript' type='text/javascript'>
		alert('Invalid US/Canada Zip!');
		</script>";
		printf("<script>location.href='". site_url() ."'</script>");
		}
	
	
	// Record what service we are on
	// Need to add functionality for Canada
	if ($services == "Pet Sitting") 
		{ $type = 19; }
	elseif ($services == "Dog Walking") 
		{ $type = 20; }
	elseif ($services == "Pet Boarding") 
		{ $type = 21; }
	elseif ($services == "Doggie Daycare") 
		{ $type = 22; }
	elseif ($services == "Dog Training") 
		{ $type = 23; }
	elseif ($services == "Pet Waste Removal") 
		{ $type = 24; }
	elseif ($services == "Pet Grooming") 
		{ $type = 25; }
	
	
	//QUERY DATABASE TO CHECK IF USERS EXISTS IN DATABASE
	$sql = "SELECT * FROM users WHERE Pet_Name = '$petsname' AND Service_Type = '$services' AND Pet_Type = '$ihave' AND Zip = '$zipcode' AND uKey = '$unique'";
	$result = $mysqli->query($sql);
	$num_rows = mysqli_num_rows($result);

	//IF USER DOESN'T ALREADY EXIST, ENTER THEM INTO THE DATABASE
	if($num_rows==0){
		$sql_insert = "INSERT INTO users (`Pet_Name`, `Service_Type`, `Pet_Type`, `Zip`, `uKey`, `IP_Address`, `TYPE`, `SRC`, `SubID`, `PubID`) VALUES ('$petsname', '$services', '$ihave', '$zipcode', '$unique', '$ip', '$type', '$src', '$subid', '$pubid')";
		$result_insert = $mysqli->query($sql_insert);

		//IF THE PET TYPE WAS CHOSEN AS 'OTHER' THEN ENTER THE OTHER TEXT INTO THE FIELD
		if($ihave=="Other"){
			$Have_Other = $mysqli->real_escape_string($_POST['Have_Other']);
			if($Have_Other){
				$sql_update_other = "UPDATE users SET Have_Other = '$Have_Other' WHERE uKey = '$unique'";
				$result_update_other = $mysqli->query($sql_update_other);
			}
		}
		
		//IF EVERYTHING WORKS, FORWARD TO SECOND FORM PAGE
		if($result_insert){
			$sql = "SELECT * FROM users WHERE `uKey` = '$unique'";
			$result = $mysqli->query($sql);
			$row = mysqli_fetch_array($result);
			$Zip = $row['Zip'];	
		}
		//IF THERE IS AN ERROR INSERTING THE DATA, GIVE AN ERROR MESSAGE
		else{
			echo "<script>alert('There was an error submitted your info. Please try again.');</script>";
			printf("<script>location.href='". site_url() ."'</script>");
		}
	}
	//IF A RECORD WITH ALL THE SAME FIELDS AND VALUES IS IN THE DATABASE GIVE AN ERROR MESSAGE
	else{
		echo "<script>alert('Your records already exist in our database.');</script>";
		printf("<script>location.href='". site_url() ."'</script>");
	}
}
// Page Title
global $h1header;
$h1header = get_field('title');
if ($services == "Pet Sitting") 
		{ $h1header = 'Find Your Pet Sitter Here!'; }
	elseif ($services == "Dog Walking") 
		{ $h1header = 'Find Your Dog Walker Here!'; }
	elseif ($services == "Pet Boarding") 
		{ $h1header = 'Find Your Pet Boarder Here!'; }
	elseif ($services == "Doggie Daycare") 
		{ $h1header = 'Find Doggie Daycare Here!'; }
	elseif ($services == "Dog Training") 
		{ $h1header = 'Find Your Dog Trainer Here!'; }
	elseif ($services == "Pet Waste Removal") 
		{ $h1header = 'Find Pet Waste Removal Here!'; }
	elseif ($services == "Pet Grooming") 
		{ $h1header = 'Find Your Pet Groomer Here!'; }

 
?>


<?php get_header(); ?>



		<div id="content" style="min-height:550px;">

<div id="page2-content">
<span id="p2-title">Step 2 of 2: Please enter your details</span>
<br /><br>
<p>Why do we need this? Your personal information will be sent in a safe and secure environment only to the pet care companies so that they can
contact you with their service offering. Read our <a href="privacy-policy">Privacy Policy</a>.</p>
<br>
<?php
		
		if ($services == "Pet Sitting"){
			include 'inc/ps.php';
		}
		elseif ($services == "Dog Walking"){
			include 'inc/ldw.php';
		}
		elseif ($services == "Pet Boarding"){
			include 'inc/pbf.php';
		}
		elseif ($services == "Doggie Daycare"){
			include 'inc/fdd.php';
		}
		elseif ($services == "Dog Training"){
			include 'inc/pdt.php';
		}
		elseif ($services == "Pet Waste Removal"){
			include 'inc/pwr.php';
		}
		elseif ($services == "Pet Grooming"){
			include 'inc/pgf.php';
		}
?>

</div>

		</div><!-- end #content --><br>&nbsp;








<?php get_footer(); ?>