<?php
/*

Template Name: Page3

Backup Aug 10th before moving api and email to bottom of page
*/
?>
<?php
include('connect.php');
$unique = session_id();

if($_POST){
	$First_Name = $mysqli->real_escape_string($_POST['First_Name']);
	$Last_Name = $mysqli->real_escape_string($_POST['Last_Name']);
	$Email = $mysqli->real_escape_string($_POST['Email']);
	$Phone1 = $mysqli->real_escape_string($_POST['Phone']);
	$Phone = str_replace('-', '', $Phone1);
	$Pet_Age = $mysqli->real_escape_string($_POST['Pet_Age']);
	$Number_Of_Pets = $mysqli->real_escape_string($_POST['Number_Of_Pets']);
	$Start_Date = $mysqli->real_escape_string($_POST['Start_Date']);
	if ($Start_Date == 'Specific Date') $date = $mysqli->real_escape_string($_POST['txtStartDate']);
	$City = $mysqli->real_escape_string($_POST['City']);
	$State = $mysqli->real_escape_string($_POST['State']);
	$Pet_Size = $mysqli->real_escape_string($_POST['Pet_Size']);
	$Promo_Code = $mysqli->real_escape_string($_POST['Promo_Code']);
	$Comments = $mysqli->real_escape_string($_POST['Comments']);
	$when = $mysqli->real_escape_string($_POST['when']);
	$desc = $mysqli->real_escape_string($_POST['desc']);
	$often = $mysqli->real_escape_string($_POST['often']);
	$API_Action = "submitLead";
	$type = $row['TYPE'];
	$ip = $row['IP_Address'];
	//change landing page
	$landing_page = "LDW";
	$landing_page = get_bloginfo ( 'description' );
	$Pet_Name = $row['Pet_Name'];
	$Pet_Type = $row['Pet_Type'];
	$Pet_Type_Other_Value = $row['Have_Other'];
	$Service_Type = $row['Service_Type'];
	

	//QUERY DATABASE TO CHECK IF USERS EXISTS IN DATABASE
		$sql = "SELECT * FROM users WHERE `Email` = '$Email'";
		$result = $mysqli->query($sql);

	$num_rows = mysqli_num_rows($result);
	
	if($num_rows==0){
		$sql_insert = "UPDATE users
		SET `First_Name` = '$First_Name',
		`Last_Name` = '$Last_Name',
		`Email` = '$Email',
		`Phone` = '$Phone',
		`Pet_Age` = '$Pet_Age',
		`Number_Of_Pets` = '$Number_Of_Pets',
		`Start_Date` = '$Start_Date',
		`City` = '$City',
		`State` = '$State',
		`Pet_Size` = '$Pet_Size',
		`Promo_Code` = '$Promo_Code',
		`Comments` = '$Comments',
		`API_Action` = '$API_Action',
		`Landing_Page` = '$landing_page',
		`date` = '$date',
		`when` = '$when',
		`desc` = '$desc',
		`often` = '$often'
		WHERE `uKey` = '$unique'";
		$result_insert = $mysqli->query($sql_insert);
		if($result_insert){
				
			//REDIRECT TO NEXT PAGE
			$sql = "SELECT * FROM users WHERE `uKey` = '$unique'";
			$result = $mysqli->query($sql);
			$row = mysqli_fetch_array($result);
			$Service_Type = $row['Service_Type'];
			
		}
	}
	else{
		echo "<script>alert('Your email already exists in the database, please try again with a different email.');</script>";
	}
	
}

//part 1 from page4
$sql = "SELECT * FROM users WHERE `uKey` = '$unique'";
$result = $mysqli->query($sql);
$row = mysqli_fetch_array($result);

//GET USER VARIABLES
$First_Name = $row['First_Name'];
$Last_Name = $row['Last_Name'];
$Email = $row['Email'];
$Phone = $row['Phone'];
$Pet_Age = $row['Pet_Age'];
$Number_Of_Pets = $row['Number_Of_Pets'];
$Start_Date = $row['Start_Date'];
$City = $row['City'];
$State = $row['State'];
$Pet_Size = $row['Pet_Size'];
$Promo_Code = $row['Promo_Code'];
$Comments = $row['Comments'];
$API_Action = "submitLead";
$type = $row['TYPE'];
$ip = $row['IP_Address'];
$src = $row['SRC'];
$landing_page = $row['Landing_Page'];
$Pet_Name = $row['Pet_Name'];
$Pet_Type = $row['Pet_Type'];
$Pet_Type_Other_Value = $row['Have_Other'];
$Service_Type = $row['Service_Type'];
$Zip = $row['Zip'];
$date = $row['date'];
$subid = $row['SubID'];
$pubid =$row['PubID'];
$when=$row['when'];
$desc=$row['desc'];
$often=$row['often'];


//Make Adjustments for Canada
$ziptext = "&Zip=";
$statetext = "&State=";
if( $type > 26){
	$ziptext = "&Postal_Code=";
	$statetext = "&Province=";

	if ($Start_Date == "Within 60 Days"){
		$Start_Date = "Price Shopping";
		
	}
}


//part 2 from page4
//MAKE BOBERDOO API CALL WITH cURL 
$url = "http://leads.petsitting.com/genericPostlead.php?Test_Lead=1&TYPE=".urlencode($type)."&IP_Address=".urlencode($ip)."&SRC=".urlencode($src)."&Landing_Page=".urlencode($landing_page)."&First_Name=".urlencode($First_Name)."&Last_Name=".urlencode($Last_Name)."&City=".urlencode($City). $statetext .urlencode($State). $ziptext . urlencode($Zip)."&Email=".urlencode($Email)."&Phone=".urlencode($Phone)."&Pet_Name=".urlencode($Pet_Name)."&Pet_Type=".urlencode($Pet_Type)."&Pet_Age=".urlencode($Pet_Age)."&Number_Of_Pets=".urlencode($Number_Of_Pets)."&Service_Type=".urlencode($Service_Type)."&Pet_Size=".urlencode($Pet_Size);

//email values
$msgform .="<tr><td>Name:</td><td>".$First_Name. " " . $Last_Name ."</td>";
$msgform .="<tr><td>City & State:</td><td>".$City. ", " . $State ."</td>";
$msgform .="<tr><td>Zip:</td><td>".$Zip . "</td>";
$msgform .="<tr><td>Phone:</td><td>".$Phone . "</td>";
$msgform .="<tr><td>Email:</td><td>".$Email . "</td>";
$msgform .="<tr><td>Type of Pet:</td><td>".$Pet_Type . "</td>";
if($Pet_Type_Other_Value != ''){
    $url .= "&Pet_Type_Other_Value=".urlencode($Pet_Type_Other_Value);
    $msgform .="<tr><td>Pet Type:</td><td>".$Pet_Type_Other_Value . "</td>";
}
$msgform .="<tr><td>Pet Name:</td><td>".$Pet_Name . "</td>";
$msgform .="<tr><td>Size:</td><td>".$Pet_Size . "</td>";
$msgform .="<tr><td>Age:</td><td>".$Pet_Age . "</td>";
$msgform .="<tr><td>Start Date:</td><td>".$Start_Date . "</td>";
if( $date != ''){
    $url .= "&Specific_Date=".urlencode($date);
    $msgform .="<tr><td>Start at Specific Date:</td><td>".$date . "</td>";
}
$msgform .="<tr><td># pets:</td><td>".$Number_Of_Pets . "</td>";

$msgform .="<tr><td>Lead Registration Date:</td><td>". date("m/d/Y") . "</td>";


//Optional codes are appended if filled in
if($Start_Date != ''){
    $url .= "&Start_Date=".urlencode($Start_Date);
}
if( $Promo_Code != ''){
    $url .= "&Promo_Code=".urlencode($Promo_Code);
}
if( $Comments != ''){
    $url .= "&Comments=".urlencode($Comments);
    $msgform .="<tr><td>Comments:</td><td>".$Comments . "</td>";
}
if( $subid != ''){
    $url .= "&Sub_ID=".urlencode($subid);
}
if( $pubid != ''){
    $url .= "&Pub_ID=".urlencode($pubid);
}
if( $when != ''){
    $url .= "&Walk_Time_Frame=".urlencode($when);
}
if( $when != '' && intval($Number_Of_Pets ) > 1){
    $url .= "&Multiple_Dogs=".urlencode('Yes');
}
if( $when != '' && intval($Number_Of_Pets ) <= 1){
    $url .= "&Multiple_Dogs=".urlencode('No');
}
if( $desc != ''){
    $url .= "&Home_Type=".urlencode($desc);
}
if( $often != ''){
    $url .= "&Service_Frequency=".urlencode($often);
}


//echo $url;
//$xml = simplexml_load_file($url);
$curl = curl_init($url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
$data = curl_exec($curl);
$raw_xml = $mysqli->real_escape_string($data);
$xml = simplexml_load_string($data);

//part 3 from page 4

    $sql = "INSERT INTO xml_response (api_request, response, datetime, unique_key) VALUES ('$url', '$raw_xml', NOW(), '$unique')";
    $result = $mysqli->query($sql);


//part 5
// insert lead no into email

foreach($xml->children() as $child)
  {
  $err[]= $child->getName();
  $leadno =$child;
  break;
  }


$leadtype = $landing_page;



// Page Title
global $h1header;

 $h1header = get_field('title');
if ($Service_Type == "Pet Sitting") 
		{ $h1header = 'Find Your Pet Sitter Here!'; 
			$leadtype .= "S";
		}
	elseif ($Service_Type == "Dog Walking") 
		{ $h1header = 'Find Your Dog Walker Here!'; $leadtype .= "D";}
	elseif ($Service_Type == "Pet Boarding") 
		{ $h1header = 'Find Your Pet Boarder Here!'; $leadtype .= "PB";}
	elseif ($Service_Type == "Doggie Daycare") 
		{ $h1header = 'Find Doggie Daycare Here!'; $leadtype .= "DD";}
	elseif ($Service_Type == "Dog Training") 
		{ $h1header = 'Find Your Dog Trainer Here!'; $leadtype .= "T";}
	elseif ($Service_Type == "Pet Waste Removal") 
		{ $h1header = 'Find Pet Waste Removal Here!'; $leadtype .= "WR";}
	elseif ($Service_Type == "Pet Grooming") 
		{ $h1header = 'Find Your Pet Groomer Here!'; $leadtype .= "G";}


if ($err[0] != 'error'){

include('inc/mailtest.php');

}

?>

<?php get_header(); ?>



		<div id="content">
<div id="upsell">
<span class="thankyouheading">Last Question</span><br><br>
<form action="<?php echo home_url(); ?>/results" method="POST">
Would you like to receive FREE discounts of up to 90% on Dog & Cat deals from Coupaw.com?<br /><br>
<table>
                                        <tbody><tr>
                                            <td style="vertical-align:middle;clear:both;">
                                            <table>
                                                <tbody><tr style="vertical-align:middle;clear:both;">
                                                <td class="getQuesRadio" style="width:70px;clear:both;">
                                                   <input type="radio" name="coupaw_upsell" value="yes" /> Yes
                                                </td>
                                                </tr>
                                                <tr>
                                                <td class="getQuesRadio" style="vertical-align:middle;clear:both;padding-top: 10px;">
                                                   <input type="radio" name="coupaw_upsell" value="no" /> No
                                                </td>
                                                
                                                </tr>
                                            </tbody></table>
                                            </td>
                                            <td>
                                                <img src="<?php echo get_template_directory_uri(); ?>/images/Coupaw_cat_dog.jpg" height="80" width="80" alt="img">
                                            </td>
                                        </tr>
                                           
                                        </tbody></table><br>


<input type="image" name="btnFinish" id="btnFinish" src="<?php echo get_stylesheet_directory_uri(); ?>/images/finalize.png">
</form>
</div>

		</div><!-- end #content -->








<?php get_footer(); ?>