<?php
/*
Template Name: Rent
*/
?>

<?php
// Grab SRC Parameters
$_GET_lower = array_change_key_case($_GET, CASE_LOWER);
$src = $_GET_lower['arc'];
if (!$src) $src=get_bloginfo('description');
$src = "rent-com";

$subid = $_GET_lower['subid'];
$pubid = $_GET_lower['pubid'];
?>

<?php get_header(rent); ?>

									
									 
									 

<!-- Write script for radio buttons-->
<script>
function validateForm()
{	var msg = "";
var x=document.forms["main"]["petsname"].value;
if (x==null || x=="")
  {  msg = msg + "Pet name must be filled out\n";  }
var x=document.forms["main"]["Zip"].value;
if (x==null || x=="")
  {  msg = msg + "You must enter a valid Zip Code!\n";  }
var x=document.forms["main"]["Have_Other"].value;
if(document.getElementById('other').checked && x.length < 1) {
	msg = msg +"Please Fill In Pet Type!\n";}
if(document.getElementById('other').checked == false &&document.getElementById('cat').checked == false &&document.getElementById('dog').checked == false ) {
	msg = msg +"Please Choose Pet Type!\n";}

if (msg != ""){
alert(msg);
return false;}
}
</script>




		<div id="content" style="min-height:662px;margin-top:0px;position:relative;">





			<div id="left">



				<div id="orderform"  style="margin-left: 160px;">
					<div id="title"> <h4>FILL BELOW TO START</h4> </div>
					<form name="main" action="<?php echo home_url(); ?>/rent-com-personal-details/#page2-content" method="post" style="margin-left:18px;padding-top:20px;" onsubmit="return validateForm()">

					<div class="box"><label for="petsname">What is your pet's name?</label><br />

					<input name="petsname" type="text" class="textbox" /><br /></div>

					<div class="box"><label for="services">What services are you seeking?</label><br />

					<select name="services" id="services">
							<option value="Pet Sitting">Pet Sitting</option>
<option value="Dog Walking">Dog Walking</option>
<option value="Pet Boarding">Pet Boarding</option>
<option value="Dog Training">Dog Training</option>
<option value="Pet Grooming">Pet Grooming</option>				
		<option value="Pet Waste Removal">Pet Waste Removal</option>
		<option value="Doggie Daycare">Doggie Daycare</option>
						


					</select><br /></div>

					<div class="box"><label for="ihave">I have a:</label><br />

						<input type="radio" name="Pet_Type" id="dog" value="Dog" style="margin-top:5px;"> Dog&nbsp;&nbsp;

						<input type="radio" name="Pet_Type" id="cat" value="Cat"> Cat<br />

						<input type="radio" name="Pet_Type" id="other" value="Other" style="margin-top:10px;"> Other &nbsp; <input type="text" name="Have_Other" class="textboxs"/><br /></div>

				<div class="box">	<label for="zipcode">Enter Zip Code or Postal Code:</label>

					<input type="text" name="Zip" class="textbox"/><br />
					<input type="text" name="SRC" class="textbox" style="display:none;" value="<?php echo $src;?>"/>
					<input type="text" name="SubID" class="textbox" style="display:none;" value="<?php echo $subid;?>"/>
					<input type="text" name="PubID" class="textbox" style="display:none;" value="<?php echo $pubid;?>"/>
					
					
					
					</div><div class="submitbox">
<input style="margin-top: 8px;margin-left: -6px;" type="submit" value="Get Quotes !" onclick="return validate_form();" class="submit"></div>
				</form>

<div id="privacy-logo">
<div id="6614d0cf-1fad-4c74-889a-0f3443137bbb"> <script type="text/javascript" src="//privacy-policy.truste.com/privacy-seal/Dice-Solutions-LLC/asc?rid=6614d0cf-1fad-4c74-889a-0f3443137bbb"></script><a href="//privacy.truste.com/privacy-seal/Dice-Solutions-LLC/validation?rid=d152df82-1e09-4637-8568-7b7da1c642f2" title="TRUSTe online privacy certification" target="_blank"><img style="border: none" src="//privacy-policy.truste.com/privacy-seal/Dice-Solutions-LLC/seal?rid=d152df82-1e09-4637-8568-7b7da1c642f2" alt="TRUSTe online privacy certification"/></a></div>
</div>



				</div>







			</div>



			<div id="right" style="margin-left: 0px;margin-top: 0px;">







							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>



									



									<h2><?php the_field('page_title'); ?></h2>



						



										<?php the_content(); ?>







										



						



							<?php endwhile; endif; ?>



			</div>



			<div id="bottom" style="width:867px;margin:auto;">
			
			

 <table cellpadding="0" cellspacing="0" width="100%">
    <tbody>
                                <tr>
                                    <td align="left" valign="top" colspan="2">
                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/hrLine.gif" alt="">
                                    </td>
                                </tr>
                          
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="bottom_nav_wrapper">
                            <table cellpadding="0" cellspacing="0" width="100%">
                            <tbody><tr>
                            <td class="bottomnavText" style="padding-left:120px">
                            
                                 <a href="/privacy-policy/" class="bottomnavTextLink">Privacy Policy</a>
                               
                                    |
                                
                                <a href="/terms-of-use/" class="bottomnavTextLink">Terms of Use</a>
                                
                                    |
                                
                                <a href="/how-it-works/" class="bottomnavTextLink">How it works</a>
								
								
                            </td>
                            <td align="right" class="bottomnavText"><a href="http://twitter.com/petsittingcom" target="_blank" class="bottomnavTextLink">Twitter</a> | <a href="http://www.facebook.com/pages/Petsittingcom/124828527550931" target="_blank" class="bottomnavTextLink">Facebook</a></td>
                            </tr>
                            </tbody></table>
                            
                            <div class="height">
                            </div>
                            <div class="bottom-nav-links">
                                <a target="_BLANK" title="Dice Solutions, LLC BBB Business Review" href="http://www.bbb.org/new-york-city/business-reviews/marketing-programs-and-services/dice-solutions-llc-in-great-neck-ny-111934/#bbbonlineclick"><img alt=" Dice Solutions, LLC BBB Business Review" border="0" src="http://ourbbbonline2.bbb.org/NYC/BBBOnlineSeal/111934/H2/0/seal.png"></a>&nbsp;&nbsp;<a href="/privacy-policy/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/truste_seal_web.gif"></a>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody></table>
			
			
			</div>



		</div><!-- end #content -->















<?php get_footer(blank); ?>